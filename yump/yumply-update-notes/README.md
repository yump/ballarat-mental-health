# Yumply Update Notes
## Please note down any changes happened in this project which you believe would be helpful to add to Yumply.

If you are making changes directly into Yumply repo, ignore this file.

### Rules are:
- Any changes to YumpModule or Yumply on a project need to be a separate commit, and the commit description should start with "Yumply:" so we can later on cherry pick into Yumply more easily.
- DO NOT commit gulp compiled files into Yumply commits (i.e. the compiled css / js files), because the compiled files include project-specific code.
- DO NOT commit composer.json, composer.lock because it can easily conflicts with Yumply repo.
- DO NOT commit package.json, package-lock.json because it can easily conflicts with Yumply
- Create a new file under yump/yumply-update-notes/[project_name].md, which logs any composer / NPM package changes and database changes. (see examples below)


12 October 2020
---
- [composer] Add a new plugin "freeform"
- [composer] Remove plugin "Field Labels" after updating
- [update] Update Craft to 3.5.12
- [update] Update NEO plugin
- [update] Update Redactor plugin
- [update] Update Retour plugin
- [update] Update SEOmatic plugin
- [update] Update Blitz plugin
- [update] Update Neo plugin
- [update] Update Visor plugin
- [update] Update Field Labels plugin

08 November 2019
---
- [composer] Add a new Craft plugin "maps"
- [composer] Add a new Craft plugin "Date Range Field"
- [composer] Add a new Craft plugin "Tags"
- [update] Update Craft to 3.3.15
- [update] Update NEO plugin
- [update] Update Retour plugin
- [update] Update SEOmatic plugin
- [update] Update Sprout Form / Report plugin

###Example entry:

30 Sep 2019
---
- [database] Add a new content block "Rock n Roll".
- [composer] Add a new package "psr/log"
- [composer] Add a new Craft plugin "nystudio/craft-instantanalytics"
- [npm] Add a new package "slick"