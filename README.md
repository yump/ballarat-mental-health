# Yumpcraft

Node version: 14.17.0

### Site Navigation Cache
- To clear the nav cache you can empty the contents of (not delete) yump\private\cache\data\site-navigation.json
- See more on cache here modules\yumpmodule\src\services\YumpModuleCacheService.php

### Site Search
Site search has been updated to accomodate both entries and assets. Also it's re-written in Vue.js.

Prerequisites
- Asset Volume in the CMS with 'documents' as handle. Also with 'title', 'summary', 'tags' as fields.
- Tags (handle: tags) field added in CMS. 
- The handle of the 'tags' field can be changed in general.php "searchTagFieldHandle"
- "searchFiltersMapping" under general.php is manually set based on different projects
- 'q' as the query parameter in the search forms. That's also the only form param needed. The filters are now handled by Vue
- You will probably need to update 'getBreadcrumb' under YumpModuleService based on the filters of the project
- You will probably need to update the filters in search.html and also site-search.js based on the filters.
- Check out the available attributes per search result item using Vue browser tool to use anything you need for a project. E.g. tags, file extension, etc
