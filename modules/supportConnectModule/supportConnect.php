<?php


namespace modules\supportConnectModule;

use Craft;
use craft\base\Element;
use craft\services\Elements;
use craft\helpers\ArrayHelper;
use craft\elements\Entry;
use craft\web\twig\variables\CraftVariable;
use craft\queue\Queue;
use modules\supportConnectModule\jobs\UpdateSupportOptionsCacheJob;
use modules\supportConnectModule\services\Supports;
use modules\supportConnectModule\services\Logs;
use modules\supportConnectModule\variables\SupportConnectVariables;
use craft\events\ElementEvent;
use craft\events\ElementQueryEvent;
use modules\yumpmodule\services\YumpModuleService;
use modules\yumpmodule\YumpModule;
use yii\base\Event;
use yii\base\Module;

//hide admin
use craft\events\RegisterElementSourcesEvent;
use craft\events\RegisterUserActionsEvent;
use craft\controllers\UsersController;
use craft\elements\User;
use yii\debug\models\search\Log;
use yii\web\ForbiddenHttpException;

class supportConnect extends Module
{

    /**
     * Copy From craft\base\Plugin
     * @inheritdoc
     */
    public function __construct($id, $parent = null, array $config = [])
    {
        if (($basePath = ArrayHelper::remove($config, 'basePath')) !== null) {
            $this->setBasePath($basePath);
        }

        // Set this as the global instance of this plugin class
        static::setInstance($this);

        // Set the default controller namespace
        if ($this->controllerNamespace === null && ($pos = strrpos(static::class, '\\')) !== false) {
            $namespace = substr(static::class, 0, $pos);
            if (Craft::$app->getRequest()->getIsConsoleRequest()) {
                $this->controllerNamespace = $namespace . '\\console\\controllers';
            } else {
                $this->controllerNamespace = $namespace . '\\controllers';
            }
        }

        parent::__construct($id, $parent, $config);
    }

    /**
     * Initializes the module.
     */
    public function init()
    {
        // Set a @modules alias pointed to the modules/ directory
        Craft::setAlias('@modules', __DIR__);

        $this->_hideAdminFromNoneAdmin();
        $this->_registerLogger();
        $this->_registerVariables();
        $this->_registerEventsToRefreshSupportOptionCache();

        parent::init();
    }


    /**
     * Custom logging config
     *
     * Usage: same as Craft logging usage:
     * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
     * Craft::info(): record a message that conveys some useful information.
     * Craft::warning(): record a warning message that indicates something unexpected has happened.
     * Craft::error(): record a fatal error that should be investigated as soon as possible.
     *
     * E.g. Craft::error('my message', __METHOD__);
     *
     */
    private function _registerLogger(){
        $fileTarget = new \craft\log\FileTarget([
            'logFile' => '@storage/logs/supportconnect.log',
            'categories' => ['modules\supportConnectModule\*']
        ]);
        // Add the new target file target to the dispatcher
        Craft::getLogger()->dispatcher->targets[] = $fileTarget;
    }

    private function _registerVariables(){
        // Register our variables
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('supportconnect', SupportConnectVariables::class);
            }
        );
    }

    /**
     * register events to hide admin from none admin user
     * copied from https://github.com/jalendport/craft-hideadmin/blob/master/src/HideAdmin.php
     */
    private function _hideAdminFromNoneAdmin(){
        Event::on(
            User::class,
            Element::EVENT_REGISTER_SOURCES,
            function(RegisterElementSourcesEvent $event) {
                $currentUser = Craft::$app->getUser()->getIdentity();

                if (!$currentUser->admin) {
                    // Reset sources
                    $event->sources = [];

                    $groups = Craft::$app->getUserGroups()->getAllGroups();

                    if (!empty($groups)) {
                        $event->sources[] = ['heading' => Craft::t('app', 'Groups')];

                        foreach ($groups as $group) {
                            $event->sources[] = [
                                'key' => 'group:'.$group->id,
                                'label' => Craft::t('site', $group->name),
                                'criteria' => ['groupId' => $group->id],
                                'hasThumbs' => true
                            ];
                        }
                    }
                }
            }
        );

        Event::on(
            UsersController::class,
            UsersController::EVENT_REGISTER_USER_ACTIONS,
            function(RegisterUserActionsEvent $event) {
                $currentUser = Craft::$app->getUser()->getIdentity();

                if (!$currentUser->admin) {
                    $user = $event->user;

                    // If request involves an admin throw exception
                    if($user->admin) {
                        throw new ForbiddenHttpException("Your account doesn't have permission to perform this action.");
                    }
                }
            }
        );
    }

    private function _registerEventsToRefreshSupportOptionCache(){
        /**
         * After entry save event
         */

        Event::on(
            Elements::class,
            Elements::EVENT_AFTER_SAVE_ELEMENT,
            function (ElementEvent $event) {
                $element = $event->element;
                $supportOptionsSectionHandle = (new YumpModuleService())->getConfig("supportOptionEntrySectionHandle");
                if($element instanceof Entry
                    && $element->getSection()->handle == $supportOptionsSectionHandle // is in 'supportOptions' section
                    && !$element->getIsRevision() // is not revision
                    && !$element->getIsDraft() // is not draft
                    && !$element->propagating // not propagating (avoid batch propagating)
                    && !$element->resaving // not resaving (avoid batch resaving)10/);
                ){
                    (new Queue())->push(new UpdateSupportOptionsCacheJob());//add it to job queue instead of execute it right away;
//                    (new Supports())->forceUpdateSupports();
//                    Logs::log($r, __METHOD__);
//                    Logs::log("Successfully triggered Support Option Save Event", __METHOD__);
                }
            }
        );

        /**
         * Avoid our auto cache update handler caught in a batch resaving action
         * @var [type]
         */
        Event::on(
            Elements::class,
            Elements::EVENT_AFTER_RESAVE_ELEMENTS,
            function (ElementQueryEvent $event) {
//                $query = $event->query;
//                 Craft::info('Entries resaved: ' . print_r($query), __METHOD__); // test
                (new Queue())->push(new UpdateSupportOptionsCacheJob());//add it to job queue instead of execute it right away;
            }
        );
    }
}
