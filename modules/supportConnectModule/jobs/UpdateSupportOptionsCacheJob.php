<?php
/**
 * @link https://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license https://craftcms.github.io/license/
 */

namespace modules\supportConnectModule\jobs;

use craft\queue\JobInterface;
use craft\queue\QueueInterface;
use modules\supportConnectModule\services\Supports;
use yii\queue\Queue;

/**
 * JobInterface defines the common interface to be implemented by job classes.
 * A class implementing this interface should also use [[SavableComponentTrait]] and [[JobTrait]].
 *
 * @author Pixel & Tonic, Inc. <support@pixelandtonic.com>
 * @since 3.0.0
 */
class UpdateSupportOptionsCacheJob implements JobInterface
{
    /**
     * Returns the description that should be used for the job.
     *
     * @return string|null
     */
    public function getDescription(){
        return 'Refresh support options entry cached json string';
    }


    /**
     * @param QueueInterface|Queue $queue
     * @return array|mixed|void
     */
    public function execute($queue){
        (new Supports())->forceUpdateSupports();
    }
}
