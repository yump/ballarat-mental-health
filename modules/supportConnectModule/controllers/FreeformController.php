<?php
namespace modules\supportConnectModule\controllers;

use Craft;
use Solspace\Freeform\Freeform;

class FreeformController extends BaseController{

    protected $allowAnonymous = true;

    public function actionGet(){
        $formHandle = $this->getParam("form");
        $form = Freeform::getInstance()->forms->getFormByHandle($formHandle);

        $return = [
            'hash'=> $form->hash,
            'honeypot'=> Freeform::getInstance()->honeypot->getHoneypot($form),
            'csrf' => [
                'name' => Craft::$app->config->general->csrfTokenName,
                'value' => Craft::$app->request->csrfToken,
            ]
        ];

//        {% set form = craft.freeform.form(craft.app.request.get('form')) %}
//        {{ {
//            hash: form.hash,
//        honeypot: craft.freeform.honeypot(form),
//        csrf: {
//                name: craft.app.config.general.csrfTokenName,
//            value: craft.app.request.csrfToken,
//        }
//    }|json_encode|raw }}

        return $this->asJson($return);
    }
}
