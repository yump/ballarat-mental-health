<?php


namespace modules\supportConnectModule\controllers;

use Craft;
use craft\elements\Category;
use craft\web\Controller;
use modules\supportConnectModule\reducer\CategoryReducer;

class BaseController extends Controller
{
    protected $allowAnonymous = true;

    public function actionGetCategories(){
        $group = Craft::$app->request->getParam("group", null);
        if(!$group){
            return [];
        }
        return $this->asJson($this->_getCategoriesByGroup($group));
    }

    public function actionGetSupportFinderCategories(){
        $groupIds = [];
        $categories = Category::find()->groupId($groupIds);
        $result = [];
        foreach ($categories->all() as $category){
            $result[] = new CategoryReducer($category);
        }
        return $result;
    }

    /**
     * @param $group string
     * @return array
     */
    private function _getCategoriesByGroup($group):array{
        $categories = Category::find()->group($group);
        $result = [];
        foreach ($categories->all() as $category){
            $result[] = new CategoryReducer($category);
        }
        return $result;
    }

    /**
     * @param $attr
     * @param string $category
     * @return mixed
     */
    public function getConfig($attr, $category = 'general'){
        return Craft::$app->config->$category->$attr;
    }

    /**
     * @param $name
     * @param null $default
     * @return mixed
     */
    protected function getParam($name, $default = null){
        return Craft::$app->request->getParam($name, $default);
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function getParams(){
        $request = Craft::$app->getRequest();
        if(!empty($request->getBodyParams())) return $request->getBodyParams();
        if(!empty($request->getQueryParams())) return $request->getQueryParams();
        return [];
    }
}
