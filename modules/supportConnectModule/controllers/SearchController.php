<?php


namespace modules\supportConnectModule\controllers;


use Craft;
use craft\elements\db\EntryQuery;
use craft\elements\Entry;
use modules\supportConnectModule\reducer\SearchReducer;
use modules\supportConnectModule\services\Logs;

class SearchController extends BaseController
{
    /**
     * @return \yii\web\Response
     */
    public function actionGet(){
        $searchString = $this->getParam("q");
        $this->_processSearchString($searchString);
        $searchQuery = Entry::find()->search($searchString)->cache(86400);
        $searchQuery->sectionId = $this->_getTypeIdsForSearch();
        return $this->asJson($this->_getReducedSearch($searchQuery));
    }

    private function _getReducedSearch(EntryQuery $searchResult){
        $result = [];

        foreach ($searchResult->all() as $index => $search){
            $temp = new SearchReducer($search);
            $temp->type = $this->_getTypeBySectionId($temp->sectionId);
            $result[] = $temp;
        }

        return $result;
    }

    private function _getTypeBySectionId($id){
        $searchFiltersMapping = $this->getConfig("searchFiltersMapping");
        foreach ($searchFiltersMapping as $key => $settings){
            if(in_array($id, $settings["ids"])){
                return $key;
            }
        }
        return null;
    }

    /**
     * Get the list of types from 'searchFiltersMapping' in general.php
     * @param string $type 'entryTypes', or 'assetVolume'
     * @return array
     */
    private function _getTypeIdsForSearch($type = 'entryType') {
        $searchFiltersMapping = $this->getConfig('searchFiltersMapping');

        $ids = [];
        if($searchFiltersMapping) {
            foreach ($searchFiltersMapping as $key => $settings) {
                if($settings['type'] == $type) {
                    $ids = array_merge($ids, $settings['ids']);
                }
            }
        }
        return $ids;
    }

    /**
     * process string turn "xxx yyy" => "'xxx yyy' OR xxx yyy"
     * for better search ability in craft cms
     * @param $search
     */
    private function _processSearchString(&$search){
        if(strpos($search, " ") !== -1){
            $originalString = $search;
//            $processedSearch = str_replace(" ", " OR ", $originalString);
            $search = "'".$originalString."'"." OR ".$originalString;
        }
    }
}
