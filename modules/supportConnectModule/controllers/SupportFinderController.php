<?php
namespace modules\supportConnectModule\controllers;

use craft\elements\Entry;
use modules\supportConnectModule\services\Supports;
use modules\supportConnectModule\reducer\SupportOptionReducer;
use modules\supportConnectModule\services\Logs;

class SupportFinderController extends BaseController
{

    /**
     * @return \yii\web\Response
     */
    public function actionGet(){
        return $this->asJson((new Supports())->getSupports());
    }
}
