<?php
namespace modules\supportConnectModule\controllers;

use craft\elements\Entry;
use modules\supportConnectModule\reducer\ResourceLibraryReducer;

class ResourceLibraryController extends BaseController
{
    /**
     * @return \yii\web\Response
     */
    public function actionGet(){
        return $this->asJson($this->_getResourceLibrary());
    }

    private function _getResourceLibrary():array{
        $supportOptionEntrySectionHandle = $this->getConfig("resourceLibraryEntrySectionHandle");
        $supportOptionEntries = Entry::find()->section($supportOptionEntrySectionHandle)->orderBy('title')->all();
        $result = [];
        foreach ($supportOptionEntries as $entry){
            $result[] = new ResourceLibraryReducer($entry);
        }
        return $result;
    }
}
