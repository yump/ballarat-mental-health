<?php
namespace modules\supportConnectModule\variables;

use Craft;
use modules\supportConnectModule\services\Logs;

class SupportConnectVariables
{
    /**
     * @param $role
     * @return mixed|string
     */
    public function getGoalFromRoleAndUrl($role){
        $pathway = $this->getPathwayFromUrl();
        if(($pathway == "1" && $role == "1") || ($pathway == "2" && $role == "2") || ($pathway == "3" && $role == "3")) {
            return "1";
        }elseif($pathway == "1" && $role == "2"){
            return "2";
        }elseif($pathway == "1" && $role == "3"){
            return "3";
        }
        return "1";
    }

    /**
     * url => "support-finder/pathway-1"
     * pathwaySegment => "pathway-1"
     * pathwaySegmentExplode => ["pathway","1"]
     * pathway => "1"
     *
     * Also set the pathway in session, if we find it
     *
     * @return string
     */
    public function getPathwayFromUrl(){
        $url = Craft::$app->request->getSegments();
        $fallbackPathway = "1";
        if(is_array($url) && sizeof($url) == 2){
            $pathway = explode("-", $url[1])[1];
            Craft::$app->session->set("pathway", $pathway);
            return $pathway;
        }
        return $fallbackPathway;
    }
}
