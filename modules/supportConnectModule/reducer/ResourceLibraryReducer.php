<?php
namespace modules\supportConnectModule\reducer;

use Craft;
use craft\elements\db\CategoryQuery;
use craft\elements\db\TagQuery;
use craft\elements\Entry;
use craft\elements\Tag;
use function GuzzleHttp\Psr7\_parse_message;
use modules\supportConnectModule\services\Logs;
use yii\debug\models\search\Log;
use yii\helpers\Console;

class ResourceLibraryReducer
{
    public function __construct(Entry $resourceLibrary){
        foreach ($this as $key=>$value){
            if(isset($resourceLibrary->$key)){
                $this->$key = $resourceLibrary->$key;
            }
        }
//        $this->getCategoryIds($resourceLibrary->resourceUserType, 'resourceUserType');
        $this->getCategoryIds($resourceLibrary->resourceCategory, 'resourceCategory');
        $this->tagsToString($resourceLibrary->tags);
        $this->setIsExternal($resourceLibrary->typeId);
    }

    /** @var string */
    public $title;

    /** @var string */
    public $url;

    /** @var string */
    public $summary;

    /** @var int */
    public $id;

    /** @var int */
    public $resourceUserType;

    /** @var int */
    public $resourceCategory;

    /** @var boolean */
    public $isExternal;

    /** @var string */
    public $tagsCombinedString = "";

    private function getCategoryIds(CategoryQuery $categoryQuery, $target){
        $this->$target = $categoryQuery->ids();
    }

    private function tagsToString(TagQuery $tags, $separator = "|"){
        $tagsArr = $tags->all();
        foreach ($tagsArr as $tag){
            $this->tagsCombinedString.= $separator.$tag->title;
        }
    }

    private function setIsExternal($typeId){
        $externalEntryTypeIds = $this->_getConfig('externalEntryTypeIds');
        $this->isExternal = in_array($typeId, $externalEntryTypeIds);
    }

    /**
     * @param $attr
     * @param string $category
     * @return mixed
     */
    private function _getConfig($attr, $category = 'general'){
        return Craft::$app->config->$category->$attr;
    }
}
