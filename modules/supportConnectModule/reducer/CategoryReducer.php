<?php

namespace modules\supportConnectModule\reducer;

use craft\elements\Category;

class CategoryReducer
{
    public function __construct(Category $category)
    {
        foreach ($this as $key => $value) {
            if (isset($category->$key)) {
                //make sure everything is string
                $this->$key = (string)$category->$key;
            }
        }

        $this->setParenId($category->parent);
    }

    /** @var string */
    public $title;

    /** @var string */
    public $label;

    /** @var string */
    public $heading;

    /** @var string */
    public $id;

    /** @var string */
    public $level;

    /** @var string */
    public $parentId;

    public function setParenId($parent){
        if($parent != null){
            //make sure everything is string
            $this->parentId = (string)$parent->id;
        }
    }
}
