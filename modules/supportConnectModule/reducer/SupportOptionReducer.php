<?php


namespace modules\supportConnectModule\reducer;

use Craft;
use craft\elements\Category;
use craft\elements\db\CategoryQuery;
use craft\elements\Entry;
use ether\simplemap\models\Map;

class SupportOptionReducer
{
    public function __construct(Entry $supportOption){
        foreach ($this as $key=>$value){
            if(isset($supportOption->$key)){
                $this->$key = $supportOption->$key;
            }
        }
        $this->getLatLang($supportOption->map);
        $this->getCategoryIds($supportOption->supportCategoriesPathway1, 'pathway1');
        $this->getCategoryIds($supportOption->supportCategoriesPathway2, 'pathway2');
        $this->getCategoryIds($supportOption->supportCategoriesPathway3, 'pathway3');
        $this->getCategoryIds($supportOption->deliveryMethod, 'deliveryMethod');
        $this->getCategoryIds($supportOption->referralType, 'referralType');
//        $this->getCategoryIds($supportOption->speciality, 'speciality');
        $this->getCategoryIds($supportOption->fees, 'fees');
        $this->getCategoryIds($supportOption->communityGroup, 'communityGroup');
        $this->getCategoryIds($supportOption->ageGroup, 'ageGroup');
    }

    /** @var string */
    public $title;

    /** @var string */
    public $url;

    /** @var boolean */
    public $featured;

    /** @var string */
    public $summary;

    /** @var int */
    public $id;

    //lat & $lang
    /** @var float */
    public $lat;
    /** @var float */
    public $lng;

    /** @var boolean */
    public $ignoreRadius;

    //pathway ids
    /** @var array of category ids */
    public $pathway1;

    /** @var array of category ids */
    public $pathway2;

    /** @var array of category ids */
    public $pathway3;

    /** @var array of category ids */
    public $deliveryMethod;

    /** @var array of category ids */
    public $referralType;

//    /** @var array of category ids */
//    public $speciality;

    /** @var array of category ids */
    public $fees;

    /** @var array of category ids */
    public $communityGroup;

    /** @var array of category ids */
    public $ageGroup;

    public function getLatLang(Map $map){
        $this->lat = $map->lat;
        $this->lng = $map->lng;
    }

    public function getCategoryIds(CategoryQuery $categoryQuery, $target){
        $this->$target = $categoryQuery->ids();
    }
}
