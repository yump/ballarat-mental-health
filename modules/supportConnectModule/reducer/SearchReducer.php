<?php


namespace modules\supportConnectModule\reducer;


use Craft;
use craft\db\Query;
use craft\db\Table;
use craft\elements\db\TagQuery;
use craft\elements\Entry;
use modules\supportConnectModule\services\Logs;

class SearchReducer
{
    public function __construct(Entry $entry){
        foreach ($this as $key=>$value){
            if(isset($entry->$key)){
                $this->$key = $entry->$key??"";
            }
        }

        $this->tagsToString($entry->tags);
        $this->setCategoryTitle($entry);
        $this->setIsExternal($entry->typeId);
    }

    /** @var string */
    public $title;

    /** @var string */
    public $url;

    /** @var string */
    public $summary;

    /** @var string */
    public $id;

    /** @var string */
    public $sectionId;

    /** @var string */
    public $type;

    /** @var boolean */
    public $isExternal;

    /** @var string */
    public $tagsCombinedString = "";

//    /** @var string */
    public $taggedCategories = [];

    private function setIsExternal($typeId){
        $externalEntryTypeIds = $this->_getConfig('externalEntryTypeIds');
        $this->isExternal = in_array($typeId, $externalEntryTypeIds);
    }

    private function tagsToString($tags, $separator = "|"){
        if($tags instanceof TagQuery){
            $tagsArr = $tags->all();
            foreach ($tagsArr as $tag){
                $this->tagsCombinedString.= $separator.$tag->title;
            }
        }
    }

    private function setCategoryTitle($entry){
        foreach ($this->_getTagFieldHandle() as $field){
            if(isset($entry[$field]) && $entry->$field->ids() != []){
                $this->taggedCategories[$field] = $this->_getTitleByElementIds($entry->$field->ids());
            }
        }
    }

    private function _getTitleByElementIds(Array $ids){
        return (new Query())
            ->from(Table::CONTENT)
            ->select(["title"])
            ->where("elementId in (".implode(",",$ids).")")
            ->column();
    }

    private function _getTagFieldHandle(){
        $fieldIds = $this->_getConfig('searchTagFieldIds');
        return (new Query())
            ->from(Table::FIELDS)
            ->select(["handle"])
            ->where("id in (".implode(",",$fieldIds).")")
            ->column();
    }

    /**
     * @param $attr
     * @param string $category
     * @return mixed
     */
    private function _getConfig($attr, $category = 'general'){
        return Craft::$app->config->$category->$attr;
    }
}
