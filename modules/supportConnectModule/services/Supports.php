<?php

namespace modules\supportConnectModule\services;

use Craft;
use modules\yumpmodule\gears\cache\adapters\Supports as SupportsAdapter;

class Supports {

    private $_cachedSupportArray;

    public function getSupports() {
        if(!$this->_cachedSupportArray) {
            // Craft::info("get fresh php array:nav", __METHOD__);
            $adapter = new SupportsAdapter();
            $this->_cachedSupportArray = $adapter->getContent();
        }
        // else {
        //     Craft::info("get CACHED php array:nav", __METHOD__);
        // }

        return $this->_cachedSupportArray;
    }

    /**
     * Force instantly update support option array
     */
    public function forceUpdateSupports() {
        $adapter = new SupportsAdapter();
        if($adapter->forceUpdateCache()) {
            Craft::info("Support option cache refreshed", __METHOD__);
        } else {
            Craft::info("Support option cache refresh not successful. Please refer to yump.log for details. ", __METHOD__);
        }
    }
}
