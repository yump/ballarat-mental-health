<?php
/**
 * This class is used to manage the cache of the site navigation.
 *
 * The key part is to define how do we get a fresh list of nav items.
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\yumpmodule\gears\cache\adapters;

use modules\yumpmodule\YumpModule;
use modules\yumpmodule\gears\Cache as CacheGears;

class Nav extends CacheGears
{
	/**
	 * In case we need certain custom settings for getFreshContent() method
	 * @var [type]
	 */
	private $_settings;

	public function __construct($settings = array()) {
		$cacheKey = YumpModule::$instance->yump->getConfig('siteNavigationCacheKey') ?: 'site-navigation'; // use 'siteNavigationCacheKey' in general.php or use 'site-navigation' as the cacheKey by default

		parent::__construct($cacheKey
			, CacheGears::CACHE_METHOD_YUMP // By default it uses Yump cache. If you want to use Craft cache instead, do it here
			, true // returnAsArray
		);

		// $this->setReturnedAsArray(true);

		$this->_settings = $settings;
	}

	public function getFreshContent() {

		$topLevelPages = \craft\elements\Entry::find()
				->section('pages')
				->limit(null)
				->level(1)
				->hideFromNavigation("not 1")
				->all();

		$navItems = $this->_getNavItems($topLevelPages);

		// if including homepage, prepend homepage to the front of the list
		if(!empty($this->_settings['includeHome'])) {
			$homepage = \craft\elements\Entry::find()
					->section('homepage')
					->one();
			if($homepage) {
				$homepageNavItem = [
					'id' => $homepage->id,
					'title' => $homepage->title,
					'url' => '/',
					'target' => null,
					'level' => 1,
					'parentId' => null,
					'descendantsIds' => array(),
					'childrenIds' => array(), // useful to determine the active state of an item in nav
					'children' => null, // this can be null or a not empty array
					// 'ancestors' => array(), // array, list of ancestors of this entry
				];
				$navItems = array_merge([$homepageNavItem], $navItems);
			}
		}

		return json_encode($navItems
			, JSON_PRETTY_PRINT // for debugging purpose only, so we can have the JSON more human readable.
		);
	}

	/**
	 * Recursively get all the navItems (in pages section).
	 *
	 * Please note, any pages with 'hideFromNavigation' turned on will have itself and its children excluded from the navItems.
	 * 
	 * @param  array of Entries $entries If passing ElementQuery / EntryQuery directly, it's probably gonna work as well, because ElementQuery is iterable? But seems like all Craft docs have ->all() / .all() function called. So we can assume it's a good practice to do so, because it might have some benefits, e.g. caching?
	 * @return array  array of the navItems on this level. It's unlikely that this function would return empty array.
	 */
	private function _getNavItems($entries, $parentId = null) {
		$navItems = [];
		foreach ($entries as $entry) {
			// get the meta of the navItem itself
			$navItem = [
				'id' => YumpModule::$instance->yump->getEntryId($entry),
				'title' => $entry->title,
				'url' => $entry->getUrl(),
				'target' => null,
				'level' => $entry->level,
				'parentId' => $parentId,
				'descendantsIds' => array(), // used to check the active state of an entry (an entry is marked as active if it's current or is ancestor of current entry)
				'childrenIds' => array(), // used to find the parent page of an entry
				'children' => null, // this can be null or a not empty array
				// 'ancestors' => $ancestorList, // array, list of ancestors of this entry
			];
			if($entry->getType()->handle == 'linkOnly') {
				$link = $entry->cta;
				$navItem['url'] = YumpModule::$instance->yump->generateValidUrl($link->getUrl());
				$navItem['target'] = $link->getTarget();
			}

			// get descendantsIds
			$navItem['descendantsIds'] = $entry->getDescendants()
										->limit(null)
										->hideFromNavigation("not 1")->ids();

			// get the children of the navItem
			$children = $entry->getChildren()
						->limit(null)
						->hideFromNavigation("not 1");
			if($children->count() !== 0) {
				$navItem['childrenIds'] = $children->ids();
				$navItem['children'] = $this->_getNavItems($children->all(), $navItem['id']); // call this function recursively to get children navItems.
			}

			// add it to the navItems list
			$navItems[] = $navItem;
		}
		return $navItems;
	}
}