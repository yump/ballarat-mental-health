<?php
/**
 * This class is used to manage the cache of the site navigation.
 *
 * The key part is to define how do we get a fresh list of nav items.
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\yumpmodule\gears\cache\adapters;

use craft\elements\Entry;
use modules\supportConnectModule\reducer\SupportOptionReducer;
use modules\yumpmodule\gears\Cache as CacheGears;
use modules\yumpmodule\services\YumpModuleService;

class Supports extends CacheGears
{

    /**
     * In case we need certain custom settings for getFreshContent() method
     * @var [type]
     */
    private $_settings;

    public function __construct($settings = array()) {
        $cacheKey = (new YumpModuleService())->getConfig('supportOptionConfigKey') ?: 'support-option'; // use 'siteNavigationCacheKey' in general.php or use 'site-navigation' as the cacheKey by default

        parent::__construct($cacheKey
            , CacheGears::CACHE_METHOD_YUMP // By default it uses Yump cache. If you want to use Craft cache instead, do it here
            , true // returnAsArray
        );
        // $this->setReturnedAsArray(true);

        $this->_settings = $settings;
    }

    public function getFreshContent() {
        return json_encode($this->_getSupportOptions(), JSON_PRETTY_PRINT);
    }

    /**
     * @return array
     */
    private function _getSupportOptions():array{
        $supportOptionEntrySectionHandle = (new YumpModuleService())->getConfig("supportOptionEntrySectionHandle");
        $supportOptionEntries = Entry::find()->section($supportOptionEntrySectionHandle)->orderBy("featured DESC, title")->all();
        $result = [];
        foreach ($supportOptionEntries as $entry){
            $result[] = new SupportOptionReducer($entry);
        }

        return $result;
    }
}
