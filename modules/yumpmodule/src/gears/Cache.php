<?php
/**
 * This is an abstract class which handles Yump Cache mechanism. 
 * 
 * To use this class:
 * - You will need to create a class extending this one. See examplemodule/gears/cache/adapters/Events for example.
 * - Create a __construct function, define your own cacheKey together with your own customisation. E.g. you might want to pass custom params in the construct function, define the cache method, set specific cache options, etc. 
 * - Define how to get fresh content by writing your own getFreshContent() function.
 * Also see examplemodule/gears/cache/adapters/Events for use case.
 *
 * For more details about Yump Cache, see: YumpModuleCacheService.php
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\yumpmodule\gears;

use Craft;
use modules\yumpmodule\YumpModule;

abstract class Cache
{
	const CACHE_METHOD_CRAFT = 'craft';
	const CACHE_METHOD_YUMP = 'yump';

	/**
	 * For the details of these properties, refer to YumpModuleCacheService.php
	 */

	private $_cacheKey;

	private $_cacheMethod;

	private $_returnAsArray;

	private $_yumpCacheOptions;

	private $_customExpiryForCraftCache;

	public function __construct($cacheKey, $cacheMethod = self::CACHE_METHOD_YUMP, $returnAsArray = false) {
		$this->_cacheKey = $cacheKey;
		$this->_cacheMethod = $cacheMethod;
		$this->_returnAsArray = $returnAsArray;
		$this->_yumpCacheOptions = array();
		$this->_customExpiryForCraftCache = false;
	}

	public function setYumpCacheOptions($options) {
		$this->_yumpCacheOptions = $options;
	}

	public function setCustomExpiryForCraftCache($customExpiry) {
		$this->_customExpiryForCraftCache = $customExpiry;
	}

	public function getCacheKey() {
		return $this->_cacheKey;
	}

	public function getCacheMethod() {
		return $this->_cacheMethod;
	}

	public function getReturnedAsArray() {
		return $this->_returnAsArray;
	}

	protected function setCacheKey($val) {
		$this->_cacheKey = $val;
	}

	protected function setCacheMethod($val) {
		$this->_cacheMethod = $val;
	}

	protected function setReturnedAsArray($bool) {
		$this->_returnAsArray = $bool;
	}

	public function getGetFreshContentSettings($settings) {
		return $this->_getFreshContentSettings;
	}

	/**
	 * Get the cached content
	 * 
	 * Note: If you want to pass in options, you will need to call 'setYumpCacheOptions' or 'setCustomExpiryForCraftCache' depending on which cache method you are using.
	 * @return [type] [description]
	 */
	public function getCachedContent() {
		if(!empty($this->_cacheKey)) {
			if($this->_cacheMethod == self::CACHE_METHOD_CRAFT) {
				return YumpModule::$instance->cache->getCraftCachedContent($this->_cacheKey, $this->_returnAsArray);
			} else if ($this->_cacheMethod == self::CACHE_METHOD_YUMP) {
				return YumpModule::$instance->cache->getYumpCachedContent($this->_cacheKey, $this->_returnAsArray, $this->_yumpCacheOptions);
			} else {
				throw new Exception("The cacheMethod is not supported!");
			}
		} else {
			throw new Exception("Empty cacheKey!");
		}
	}

	public function setCachedContent($content) {
		if(!empty($this->_cacheKey)) {
			if($this->_cacheMethod == self::CACHE_METHOD_CRAFT) {
				return YumpModule::$instance->cache->setCraftCachedContent($this->_cacheKey, $content, $this->_customExpiryForCraftCache);
			} else if ($this->_cacheMethod == self::CACHE_METHOD_YUMP) {
				return YumpModule::$instance->cache->setYumpCachedContent($this->_cacheKey, $content, $this->_yumpCacheOptions);
			} else {
				throw new Exception("The cacheMethod is not supported!");
			}
		} else {
			throw new Exception("Empty cacheKey!");
		}
	}

	/**
	 * Defines how to get a fresh content. A reminder that the content MUST be json encoded
	 * 
	 * Each class extends this class should have their own getFreshContent() function defined.
	 *  
	 * @return [type] [description]
	 */
	abstract protected function getFreshContent();

	public function getContent() {
		$content = $this->getCachedContent();

		if(empty($content)) {
			$content = $this->getFreshContent();
			$this->setCachedContent($content);

			if($this->_returnAsArray && is_string($content)) {
				$content = YumpModule::$instance->yump->yump_json_decode($content, "Failed to json_decode the data from Craft cache content. (happens right after setting up cache for the first time)");
			}
		}

		return $content;
	}

	/**
	 * Force the cache to be updated.
	 * @return boolean indicates success or fail of setting new cache
	 */
	public function forceUpdateCache() {
		$content = $this->getFreshContent();
		return $this->setCachedContent($content);
	}
}