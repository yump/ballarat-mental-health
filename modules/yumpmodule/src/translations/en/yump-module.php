<?php
/**
 * yump module for Craft CMS 3.x
 *
 * Yump module for Craft 3
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

/**
 * yump en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('yump-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Yump
 * @package   YumpModule
 * @since     1.0.0
 */
return [
    'yump plugin loaded' => 'yump plugin loaded',
];
