<?php
/**
 * yump module for Craft CMS 3.x
 *
 * Yump module for Craft 3
 *
 * @link      https://yump.com.au
 * @copyright Copyright (c) 2019 Yump
 */

namespace modules\yumpmodule;

use modules\yumpmodule\assetbundles\yumpmodule\YumpModuleAsset;
use modules\yumpmodule\services\YumpModuleService as YumpModuleServiceService;
use modules\yumpmodule\services\YumpModuleBlackboxService;
use modules\yumpmodule\services\YumpModuleCacheService;
use modules\yumpmodule\variables\YumpModuleVariable;
use modules\yumpmodule\twigextensions\YumpModuleTwigExtension;
use modules\yumpmodule\fields\YumpModuleField as YumpModuleFieldField;
use modules\yumpmodule\widgets\YumpModuleWidget as YumpModuleWidgetWidget;

use Craft;
use craft\events\RegisterTemplateRootsEvent;
use craft\events\TemplateEvent;
use craft\i18n\PhpMessageSource;
use craft\web\View;
use craft\console\Application as ConsoleApplication;
use craft\web\UrlManager;
use craft\services\Elements;
use craft\services\Fields;
use craft\web\twig\variables\CraftVariable;
use craft\services\Dashboard;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\base\Module;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Yump
 * @package   YumpModule
 * @since     1.0.0
 *
 * @property  YumpModuleServiceService $yumpModuleService
 * @property  YumpModuleBlackboxService $YumpModuleBlackboxService
 * @property  YumpModuleCacheService $YumpModuleCacheService
 */
class YumpModule extends Module
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this module class so that it can be accessed via
     * YumpModule::$instance
     *
     * @var YumpModule
     */
    public static $instance;

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, array $config = [])
    {
        Craft::setAlias('@modules/yumpmodule', $this->getBasePath());
        $this->controllerNamespace = 'modules\yumpmodule\controllers';

        // Translation category
        $i18n = Craft::$app->getI18n();
        /** @noinspection UnSafeIsSetOverArrayInspection */
        if (!isset($i18n->translations[$id]) && !isset($i18n->translations[$id.'*'])) {
            $i18n->translations[$id] = [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en-US',
                'basePath' => '@modules/yumpmodule/translations',
                'forceTranslation' => true,
                'allowOverrides' => true,
            ];
        }

        // Base template directory
        Event::on(View::class, View::EVENT_REGISTER_CP_TEMPLATE_ROOTS, function (RegisterTemplateRootsEvent $e) {
            if (is_dir($baseDir = $this->getBasePath().DIRECTORY_SEPARATOR.'templates')) {
                $e->roots[$this->id] = $baseDir;
            }
        });

        // Set this as the global instance of this module class
        static::setInstance($this);

        parent::__construct($id, $parent, $config);
    }

    /**
     * Set our $instance static property to this class so that it can be accessed via
     * YumpModule::$instance
     *
     * Called after the module class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$instance = $this;

        // Load our AssetBundle
        if (Craft::$app->getRequest()->getIsCpRequest()) {
            Event::on(
                View::class,
                View::EVENT_BEFORE_RENDER_TEMPLATE,
                function (TemplateEvent $event) {
                    try {
                        Craft::$app->getView()->registerAssetBundle(YumpModuleAsset::class);
                    } catch (InvalidConfigException $e) {
                        Craft::error(
                            'Error registering AssetBundle - '.$e->getMessage(),
                            __METHOD__
                        );
                    }
                }
            );
        }

        // Add in our Twig extensions
        Craft::$app->view->registerTwigExtension(new YumpModuleTwigExtension());

        // Add in our console commands
        if (Craft::$app instanceof ConsoleApplication) {
            $this->controllerNamespace = 'modules\yumpmodule\console\controllers';
        }

        // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['siteActionTrigger1'] = 'modules/yump-module/default';
            }
        );

        // Register our CP routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['cpActionTrigger1'] = 'modules/yump-module/default/do-something';
            }
        );

        // Register our elements
        Event::on(
            Elements::class,
            Elements::EVENT_REGISTER_ELEMENT_TYPES,
            function (RegisterComponentTypesEvent $event) {
            }
        );

        // Register our fields
        Event::on(
            Fields::class,
            Fields::EVENT_REGISTER_FIELD_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = YumpModuleFieldField::class;
            }
        );

        // Register our widgets
        Event::on(
            Dashboard::class,
            Dashboard::EVENT_REGISTER_WIDGET_TYPES,
            function (RegisterComponentTypesEvent $event) {
                $event->types[] = YumpModuleWidgetWidget::class;
            }
        );

        // Register our variables
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('yump', YumpModuleVariable::class);
            }
        );

/**
 * Logging in Craft involves using one of the following methods:
 *
 * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
 * Craft::info(): record a message that conveys some useful information.
 * Craft::warning(): record a warning message that indicates something unexpected has happened.
 * Craft::error(): record a fatal error that should be investigated as soon as possible.
 *
 * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
 *
 * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
 * the category to the method (prefixed with the fully qualified class name) where the constant appears.
 *
 * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
 * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
 *
 * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
 */
        // Craft::info(
        //     Craft::t(
        //         'yump-module',
        //         '{name} module loaded',
        //         ['name' => 'yump']
        //     ),
        //     __METHOD__
        // );


        /**
         * Custom logging config
         *
         * Usage: same as Craft logging usage:
         * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
         * Craft::info(): record a message that conveys some useful information.
         * Craft::warning(): record a warning message that indicates something unexpected has happened.
         * Craft::error(): record a fatal error that should be investigated as soon as possible.
         *
         * E.g. Craft::error('my message', __METHOD__);
         * 
         */
        // Create a new file target
        $fileTarget = new \craft\log\FileTarget([
            'logFile' => '@storage/logs/yump.log',
            'categories' => ['modules\yumpmodule\*']
        ]);
        // Add the new target file target to the dispatcher
        Craft::getLogger()->dispatcher->targets[] = $fileTarget;


        // Custom Event listeners
        // =========================================================================
        /**
         * Craft 3 doesn't have a general documentation for this like Craft 2. You will mostly need to find the events in their API doc: https://docs.craftcms.com/api/v3/craft-events-assetevent.html

            There's a Stack Exchange post: https://craftcms.stackexchange.com/questions/29607/events-documentation-for-craft-3

            Also, for most element-related events, it's more useful to refer to the craft/services/elements code directly, where you can find all the element-related events, how Craft triggers them, and what params are passed into them.
         */
        
        /**
         * After entry save event
         */
        Event::on(
            \craft\services\Elements::class,
            \craft\services\Elements::EVENT_AFTER_SAVE_ELEMENT,
            function (\craft\events\ElementEvent $event) {
                $element = $event->element;
                if(
                    $element instanceof \craft\elements\Entry // is entry
                    and $element->getSection()->handle == 'pages' // is in 'pages' section
                    and !$element->getIsRevision() // is not revision
                    and !$element->getIsDraft() // is not draft
                    and !$element->propagating // not propagating (avoid batch propagating)
                    and !$element->resaving // not resaving (avoid batch resaving)10/
                ) {
                    Craft::info('Entry saved: ' . $element->title, __METHOD__); // test
                    
                    // // debugging code to find out the trace (we used to get this event handler executed twice, and we used this debugging code to find out one was triggered by saving revision.)
                    // try {
                    //     throw new \Exception();
                    // } catch (\Exception $e) {
                    //     Craft::debug(
                    //         'EVENT_AFTER_SAVE_ELEMENT '.$e->getTraceAsString(),
                    //         __METHOD__
                    //     );
                    // }
                    

                    // --------------- My Stuff -------------- //
                    // $entry is the $element
                    // $isNew = $event->isNew
                    
                    // ------ Auto flush the nav cache ------ //
                    YumpModule::$instance->yump->forceUpdateNav();
                }
            }
        );

        /**
         * Avoid our auto cache update handler caught in a batch resaving action
         * @var [type]
         */
        Event::on(
            \craft\services\Elements::class,
            \craft\services\Elements::EVENT_AFTER_RESAVE_ELEMENTS,
            function (\craft\events\ElementQueryEvent $event) {
                $query = $event->query;
                // Craft::info('Entries resaved: ' . print_r($query), __METHOD__); // test
                YumpModule::$instance->yump->forceUpdateNav();
            }
        );
        
        // Event::on(
        //     \craft\elements\Entry::class,
        //     \craft\elements\Entry::EVENT_AFTER_SAVE,
        //     function (\craft\events\ModelEvent $event) {
        //         Craft::debug(
        //             '\craft\elements\Entry::EVENT_AFTER_SAVE',
        //             __METHOD__
        //         );
        //     }
        // );

    }

    // Protected Methods
    // =========================================================================
}
