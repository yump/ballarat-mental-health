### What is this?
This folder contains files of UI Templates that's used in craft field layout UI Template.
Those template are capable of having UI links and descriptions in craft cms's instruction.

### File naming
When naming the file, please using it's entry.type, e.g. homepage-link-to-global.twig
If it's used in field layout of neo block, please name it with it's field handle, 
e.g. anchor-links-instruction.twig
