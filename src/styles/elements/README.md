## What are Elements?
Elements are everything that isn't added directly to the main entry from the backend. So a menu, footer, header, breadcrumbs. E.g. something set up in Globals in the backend.