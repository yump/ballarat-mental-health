$('.yump-carousel-outer').on('init', function(event, slick){
	// $(this).css({"position": "relative", "visibility": "visible"}); // this approach actually break things on android devices. After testing, we found out the time that the slides get messed up while loading is pretty short. I believe we don't really this this.

	/*
	Making Slick Carousels Accessible
	*/
	//Using the heading as the described by target in Carousels
	$('.yump-carousel-item-text-heading').each(function () {
	    var $slide = $(this).closest('.yump-carousel-cell');  
	    if ($slide.attr('aria-describedby') != undefined) { // ignore extra/cloned slides
	        $(this).attr('id', $slide.attr('aria-describedby'));
	    }
	});
	//If there are dots - slick puts the ID on the dots - need to remove those as we use the title above 
	$('.slick-dots li').each(function(){
		$(this).attr('id','')
	});

});

$(function() {
	initVisibleCarousel();
});

function initVisibleCarousel() {
	$('.yump-carousel-outer').not('.neo-tab-body.hide .yump-carousel-outer').each(function() {
		var $carouselContainer = $('#' + $(this).attr('id'));
		initCarousel($carouselContainer);
	});
}

function initCarousel($carouselContainer) {
	var $carousel = $carouselContainer.find('.yump-carousel');
	if($carousel.hasClass('slick-initialized')) {
		$carousel.slick('setPosition');
		return;
	}

	var autoplay = $carousel.attr('data-noautoplay') ? false : true;

	$carousel.slick({
	    infinite: true,
	    slidesToShow: 5,
	    appendArrows: $carouselContainer,
	    arrows: true,
	    autoplay: autoplay,
	    autoplaySpeed: 5000,
	    slidesToScroll: 5,
	    prevArrow: '<a type="button" class="slick-prev slick-arrow"><img src="/assets/img/png/arrow-left.png" alt="Previous Slide"></a>',
	    nextArrow: '<a type="button" class="slick-next slick-arrow"><img src="/assets/img/png/arrow-right.png" alt="Next Slide"></a>',
	    responsive: [
			{
		      breakpoint: 1199,
		      settings: {
		        slidesToShow: 4,
		        slidesToScroll: 4,
		      }
		    },
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 359,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    },
	    ],
	});

}
