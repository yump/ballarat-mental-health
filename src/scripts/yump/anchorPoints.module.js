import {scrollToElement} from '../services/utility.js';
+$(function () {
    $("[data-action='anchor-link']").each(function () {
        $(this).click(function () {
            const urlLink = $(this).data("link");
            scrollToElement(urlLink);
        });
    });

    if(typeof window.location.hash !== "undefined" && window.location.hash !== ""){
        scrollToElement(window.location.hash);
    }
});
