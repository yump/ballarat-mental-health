/**
 * Filtering
 **/

function updateQuery(checkboxs) {
    //console.log(checkboxes);
    var uri = location.href;
    var updatedURI = uri;
    var value = 1;
    var key = '';
    // console.log(uri);
    $(checkboxes).each(function (index, checkbox) {
        key = checkbox.value;

        if (checkbox.checked) {
            value = 1;
            updatedURI = updateQueryStringParameter(updatedURI, key, value);
        } else {
            value = 0;
            updatedURI = updateQueryStringParameter(updatedURI, key, value);
        }
    });
    redirectPage(updatedURI);
}

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        return uri + separator + key + "=" + value;
    }
}

function redirectPage(uri) {
    document.location.replace(uri);
}

var checkboxes = $('.refine-search-controls .form-check-input');

$('#search-button').click(function () {
    updateQuery($(checkboxes));
});