const slick = require('slick-carousel');
const debounce = require('debounce');

$(document).ready(function($){
    const imageSliderWrap = $('.image-slider-wrap');
    imageSliderWrap.each(function() {
		const self = this;
		$(self).on('init', function(event, slick){
			resetImageSlick(self, slick);
            const activeIndex = slick.slickCurrentSlide();
            updateCaption($(self), activeIndex);
		});
	});

    if(imageSliderWrap.length){
    // imageSliderWrap.on('init', function(event, slick){
    //     var $currentSlide = $(this).find('.slick-slide.slick-current');
    //     updateHasCaptionClass($(this), $currentSlide);
    // });
    // imageSliderWrap.on('beforeChange', function(event, slick, currentSlide, nextSlide){
    //     var $nextSlide = $(this).find('.slick-slide[data-slick-index="' + nextSlide + '"]');
    //     updateHasCaptionClass($(this), $nextSlide);
    // });
        imageSliderWrap.slick({
            dots: true,
            // autoplay: true,
            pauseOnHover: true,
            pauseOnDotsHover: true,
            autoplaySpeed: 5000,
            adaptiveHeight: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '<button class="btn slider-arrow arrow-previous" aria-label="Previous slide"><img src="/assets/img/svg/ic-chevron-right-large.svg" alt="previous slide" aria-hidden="true"></button>',
            nextArrow: '<button class="btn slider-arrow arrow-next" aria-label="Next slide"><img src="/assets/img/svg/ic-chevron-right-large.svg" alt="next slide" aria-hidden="true"></button>',
        });

        imageSliderWrap.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            // console.log(nextSlide);
            updateCaption($(this), nextSlide);
        });
    }

    $(window).on('resize', debounce(function() {
    	$('.image-slider-wrap').each(function() {
    		if($(this).hasClass('slick-initialized')) {
    			resetImageSlick(this, $(this).slick('getSlick'));
    		}
    	});
	}, 250));

    // // turn off autoplay to avoid page jumping after scrolling passes a certain point.
    // $(window).on('scroll', function() {
    //     $('.image-slider-wrap').each(function() {
    //         if($(this).hasClass('slick-initialized')) {
    //             var slick = $(this).slick('getSlick');
    //             if($(window).scrollTop() > $(this).offset().top + $(this).find('.slick-list').height() / 2) {
    //                 if(slick.slickGetOption('autoplay')) {
    //                     slick.slickSetOption('autoplay', false, true);
    //                 }
    //             } else {
    //                 if(!slick.slickGetOption('autoplay')) {
    //                     slick.slickSetOption('autoplay', true, true);
    //                 }
    //             }
    //         }
    //     });
    // }).trigger('scroll');
});

function resetImageSlick(target, slick) {
	slick.setPosition();
	$(target).css({'visibility': 'visible', 'position': 'relative'});
}

function updateCaption($slider, activeIndex) {
    const $parent = $slider.closest('.slider');
    const $activeSlide = $slider.find('.slick-slide[data-slick-index="' + activeIndex + '"]');
    let $caption = '';

    //handle situation that we didn't find a caption
    if($activeSlide.find('.slider-item__caption').length !== 0){
        $caption += $activeSlide.find('.slider-item__caption')[0].outerHTML;
    }else {
        $caption += '<div class="slider-item__caption" aria-hidden="true"></div>';
    }

    //handle situation that we didn't find a count
    if($activeSlide.find('.slider-item__count').length !== 0){
        $caption += $activeSlide.find('.slider-item__count')[0].outerHTML;
    }

    if($caption.length) {
        $parent.find('[data-action="caption-container"]').html($caption).slideDown();
    } else {
        $parent.find('[data-action="caption-container"]').slideUp().empty();
    }
}

/**
 * This class 'current-slide-has-caption' is for us to determine the position of the dots, otherwise it might be overlapped with the caption text
 * @param  {[type]} $slider [description]
 * @param  {[type]} $slide  [description]
 * @return {[type]}         [description]
 */
// function updateHasCaptionClass($slider, $slide) {
//     if($slide.find('.slider-item__has-caption').length) {
//         $slider.addClass('current-slide-has-caption');
//     } else {
//         $slider.removeClass('current-slide-has-caption');
//     }
// }
