import {
    catchmentPostCode,
    baseAxiosPath,
    getUrlParam,
    updateUrlParams,
    getPathway,
    filterSupportOptions,
    capSupportOptions,
} from "./services/supports";

import{
    capIncrement,
    validCap,
    scrollToElement
} from './services/utility';
// import {initSidebar} from "./services/side-sticky-callback";

+$(function () {
    const urlParam = getUrlParam();

    window.results = new Vue({
        el: '#support-results-vue',
        delimiters: ['{*', '*}'],
        data: {
            pathway: getPathway(),
            params: urlParam,
            inCatchment: urlParam.postcode?catchmentPostCode.includes(urlParam.postcode):false,
            categories: {
                supportCategory: [],
                ageGroup: [],
                communityGroup: [],
                deliveryMethod: [],
                referralType: [],
                fees: [],
                // speciality: [],
            },
            supportOptions: [],
            filteredSupportOptions: [],
            cappedSupportOptions: [],
            initialising: {
                all: true,
                update: true,
                supportOptions: true,
                supportCategory: true,
                ageGroup: true,
                communityGroup: true,
                deliveryMethod: true,
                referralType: true,
                fees: true,
                // speciality: true,
            },
            error: null,
        },
        methods: {
            checkboxUpdateCategory: function(event, categoryName, paramsName){
                this.categories[categoryName].map(e=>{
                    if(e.id === event.target.id || e.parentId === event.target.id){
                        if(event.target.checked){
                            //todo: check parent when all the children are checked
                            this.params[paramsName].push(e.id);
                        }else{
                            //uncheck parent if any child is unchecked, because we only check parent when all the children are checked
                            this.params[paramsName] = this.params[paramsName].filter(el=>el !== e.id && el !== e.parentId);
                        }
                    }
                });
            },
            loadMore: function () {
                if(this.params.cap === null ) this.params.cap = 6;
                if((this.params.cap + capIncrement) < this.filteredSupportOptions.length) {
                    this.params.cap += capIncrement;
                } else {
                    this.params.cap = validCap(this.filteredSupportOptions.length);
                }
                this.cappedSupportOptions = capSupportOptions(this.params, this.filteredSupportOptions);
                this.$nextTick(()=>this.focusNextElement());
            },
            updateResults: function (reInitCap = true, loadingDelay = true) {
                // this.params = this.tempParams;
                if(reInitCap) this.params.cap = null;
                if(loadingDelay){
                    this.initialising.update = true;
                    //manually set a 0.5 second delay, creating a loading effect
                    setTimeout(()=>{
                        //we always update it already, don't need to reload it.
                        this.filteredSupportOptions = filterSupportOptions(this.params, this.supportOptions, this.pathway,false);
                        this.cappedSupportOptions = capSupportOptions(this.params, this.filteredSupportOptions);
                        this.initialising.update = false;
						const resultsCountInBanner = document.querySelector("#bannerResultsCount");
						resultsCountInBanner.innerHTML = this.filteredSupportOptions.length;
						$('.support-results__header').addClass('support-results__header--show');
                    }, 500);
                } else {
                    this.filteredSupportOptions = filterSupportOptions(this.params, this.supportOptions, this.pathway,false);
                    this.cappedSupportOptions = capSupportOptions(this.params, this.filteredSupportOptions);
					const resultsCountInBanner = document.querySelector("#bannerResultsCount");
					resultsCountInBanner.innerHTML = this.filteredSupportOptions.length;
					$('.support-results__header').addClass('support-results__header--show');
                }
            },
            clearFilterByName: function (valName) {
                if(Array.isArray(valName)){
                    valName.map(e=>{
                        this.params[e] = Array.isArray(this.params[e])? []: null;
                    });
                }else{
                    this.params[valName] = Array.isArray(this.params[valName])? []: null;
                }
            },
            getSupportTypesNextSectionId: function (categoryId, idPrefix, isMobile) {
                const categories = this.categories.supportCategory;
                let currentIndex = 0;
                if(isMobile){
                    idPrefix+= "mobile-";
                }
                if(categories.length !== 0){
                    for(let i = 0; i< categories.length; i++){
                        if(categories[i].level === '1'){
                            if(categories[i].id === categoryId){
                                currentIndex = i;
                            }
                            if(currentIndex !== 0 && currentIndex < i){
                                return idPrefix+categories[i].id;
                            }
                        }
                    }
                }
                if(isMobile){
                    return "#accordion-filter-parent-about-me";
                }

                return "#footer-modal-support-types";
            },
            focusNextElement: function () {
                const vueInstance = this;
                const list = document.getElementById("support-results-listing-list");
                if(list){
                    const focusIndex = parseInt(vueInstance.params.cap) - 6;
                    list.childNodes.forEach(function (e, i) {
                        if(i === focusIndex){
                            const link = e.firstChild;
                            link.focus();
                            scrollToElement(link);
                        }
                    });
                }
            }
        },
        watch: {
            params: {
                handler: function(newVal) {
                    updateUrlParams(newVal);
                    // this.updateResults(); //only update result when user click on update button
					const resultsCountInBanner = document.querySelector("#bannerResultsCount");
					resultsCountInBanner.innerHTML = this.filteredSupportOptions.length;
					$('.support-results__header').addClass('support-results__header--show');
                    this.error = null;//refresh error when an update is succeed;
                },
                deep: true,
            }
        },
        mounted() {
            //make sure tooltip is initiated when bootstrap modal popped up.
            const vueInstance = this;
            $('[data-toggle="tooltip"]').tooltip();
            $('.modal').each(function(){
                $(this).on('shown.bs.modal', function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
                $(this).on('hide.bs.modal', function (e) {
                    const modalId = e.currentTarget.attributes.id.value;
                    //prevent some modal closing to reload item
                    if(modalId !== "modal-suggest-edit" && modalId !== "header-request-a-callback"){
                        vueInstance.initialising.update = true;
                        setTimeout(()=>{
                            vueInstance.initialising.update = false;
                        }, 500);
                    }
                });
            });

            //load pathway categories
            if(this.pathway){
                axios.get(baseAxiosPath +"get-categories?group=supportCategoryPathway"+ this.pathway)
                    .then(response=>{
                        this.categories.supportCategory=response.data;
                    })
                    .catch(this.onError)
                    .finally(()=>{this.initialising.supportCategory = false;});
            }

            //load support options
            axios.get(baseAxiosPath+"get")
                .then(response => {
                    this.supportOptions = response.data;
                    this.updateResults(false, false);
                    this.initialising.update = false;
                    // initSidebar();
                })
                .catch(this.onError)
                .finally(() => {
                    this.initialising.supportOptions = false;
                });

            //load age group
            axios.get(baseAxiosPath + "get-categories?group=ageGroup")
                .then(response=>this.categories.ageGroup=response.data)
                .catch(this.onError)
                .finally(()=>this.initialising.ageGroup = false);

            //load community group
            axios.get(baseAxiosPath + "get-categories?group=communityGroup")
                .then(response=>this.categories.communityGroup=response.data)
                .catch(this.onError)
                .finally(()=>this.initialising.communityGroup = false);

            //load deliveryMethod group
            axios.get(baseAxiosPath + "get-categories?group=deliveryMethod")
                .then(response=>{
                    this.categories.deliveryMethod=response.data;
                })
                .catch(this.onError)
                .finally(()=>this.initialising.deliveryMethod = false);

            //load referralType group
            axios.get(baseAxiosPath + "get-categories?group=referralType")
                .then(response=>{
                    this.categories.referralType=response.data;
                })
                .catch(this.onError)
                .finally(()=>this.initialising.referralType = false);

            //load fees group
            axios.get(baseAxiosPath + "get-categories?group=fees")
                .then(response=>{
                    this.categories.fees=response.data;
                })
                .catch(this.onError)
                .finally(()=>this.initialising.fees = false);

            //load Specialty group
            // axios.get(baseAxiosPath + "get-categories?group=speciality")
            //     .then(response=>{
            //         this.categories.speciality=response.data;
            //     })
            //     .catch(this.onError)
            //     .finally(()=>this.initialising.speciality = false);
        }
    });

});
