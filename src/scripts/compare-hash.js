+$(function () {
    $('[data-action="compare-hash"]').each(function () {
        const hash = $(this).attr('href');
        if(hash === window.location.hash ){
            $(this).parent().addClass('current');
        }

        $(this).click(function () {
            //scroll to position
            const urlLink = $(this).data("link");
            const pos = $(urlLink).offset().top - 230;
            $('html, body').animate({
               scrollTop: pos
            }, 800);

            //add or remove current class
            $(this).parent().addClass('current');
            $(this).parent().siblings().each(function () {
               if($(this).hasClass('current')){
                   $(this).removeClass('current');
               }
            });
        });
    });
});
