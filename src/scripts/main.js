import './yump/yump.basics.module.js';
import './yump/jquery.fix.module.js';
import 'popper.js';//bootstrap tooltips
import 'bootstrap';

import 'smartmenus';
import 'smartmenus-bootstrap-4';

// //////////////
// e.g. import './yump/test.module.js' (see /src/scripts/README.md for details of module.js)
import './yump/image-slider.module.js';
import './yump/jquery.counterup.module.js';
import './yump/jquery.waypoints.module.js';
import './yump/carousel.module.js';
import './yump/impact-stats.module.js';
import './yump/anchorPoints.module.js';

// project specific module
import './supportconnect/download.module.js';
import './supportconnect/sitewideAlert.module.js';
import './supportconnect/header.module.js';
import './supportconnect/quickExit.module.js';
import './supportconnect/printDocument.module.js';
