import {baseAxiosPath, catchmentPostCode, getUrlParam, updateUrlParams, getPathway, filterSupportOptions} from "./services/supports";
// import {initSidebar} from "./services/side-sticky-callback";

/**
 * Using the current filter params redirect user to search page
 * @returns {string}
 */
const getSearchUrl = () => {
   const path = (new URL(document.location)).pathname;
   const searchParam = (new URL(document.location)).search;
   const origin = (new URL(document.location)).origin;
   // console.log(path);
   const pathname = path.split("/");//['','find-support','pathway-1']
   return origin + "/support-results/"+ pathname[2] + searchParam;
};

/**
 * scroll directly to question
 * @param step
 */
const scrollToViewOfStep = (step) => {
   const element = document.querySelector('.support-finder__card--q'+ step);
   if(element){
      const yOffset = -150; //offset 150;
      const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;
      window.scrollTo({top:y, behavior: 'smooth'});
   }
};

+$(function (){
   const urlParam = getUrlParam();

   new Vue({
      el: '#support-finder-vue',
      delimiters: ['{*', '*}'],
      data: {
         pathway: getPathway(),
         searchUrl: getSearchUrl(),
         params: urlParam,
         inCatchment: urlParam.postcode?catchmentPostCode.includes(urlParam.postcode):false,
         categories: {
            supportCategory: [],
            ageGroup: [],
            communityGroup: []
         },
         supportOptions: [],
         countFilteredSupportOptions: null,
         initialising: {
            mounting: true,
            supportOptions: true,
            supportCategory: true,
            ageGroup: true,
            communityGroup: true,
         },
         error: null,
         scrollToForm: false,
      },
      methods: {
         q1: function(event) {
            this.categories.supportCategory.map(e=>{
               if(e.id === event.target.id || e.parentId === event.target.id){
                  if(event.target.checked){
                     this.params.categories.push(e.id);
                  }else{
                     this.params.categories = this.params.categories.filter(el=>el !== e.id);
                  }
               }
            });
         },
         q2radius: function(event) {
            this.params.radius = event.target.value;
         },
         q2postcode: function(event) {
            //if it looks like a postcode we update it
            if(/^\d{4}$/.test(event.target.value)){
               this.params.postcode = event.target.value;
            }else{
               autocomplete.addListener("place_changed", this.q2fillInPostcode);
            }
         },
         q2fillInPostcode: function(){
            const place = autocomplete.getPlace();

            let postcode = "";
            if(typeof place !== "undefined"){
               for (const component of place.address_components) {
                  const componentType = component.types[0];
                  if(componentType === "postal_code") {
                     postcode = `${component.long_name}${postcode}`;
                  }
               }
            }
            if(postcode !== "") {
               this.params.postcode = postcode;
               //if postcode is outside catchment area, we prompt q2 error in advance
               this.inCatchment = catchmentPostCode.includes(postcode);
               if(!this.inCatchment){
                  this.params.step = 3;
               }else{
                  // console.log("initiating radius");
                  this.params.radius = '20';//initiate radius to be 20km, when postcode selected
                  if(typeof place != "undefined" && typeof place.geometry != "undefined"){
                     this.params.lat = place.geometry.location.lat();
                     this.params.lng = place.geometry.location.lng();
                  }
               }
            }else {
               this.resetPostcodeToEmpty();
               this.error = "The address you typed doesn't have a specific postcode, trying to narrow it down. Or type a postcode directly.";
            }
         },
         q3: function(event) {
            this.params.age = event.target.value;
         },
         q3review: function() {
            if(params.age !== null){
               const selectedGroup = this.categories.ageGroup.find(e=>e.id === this.params.age);
               if(selectedGroup.length !== 0){
                  return selectedGroup[0].title;
               }
            }
         },
         q4: function(event) {
            this.categories.communityGroup.map(e=>{
               if(e.id === event.target.id || e.parentId === event.target.id){
                  if(event.target.checked){
                     this.params.community.push(e.id);
                  }else{
                     this.params.community = this.params.community.filter(el=>el !== e.id);
                  }
               }
            });
         },
         nextStep: function () {
            switch (this.params.step) {
               case 1:
                  if(this.params.categories.length === 0){
                     this.error = "Please select at least one category for support option."
                  }else {
                     this.params.step = 2;
                  }
                  break;
               case 2:
                  if(this.params.postcode === null) {
                     this.error = "Please provide a valid postcode.";
                  // }else if(this.params.lat === null || this.params.lng === null){
                  //    this.error = "Please enter an address not just a postcode.";
                  }else{
                     this.inCatchment = catchmentPostCode.includes(this.params.postcode);
                     //get default radius from html
                     if(this.params.radius === null) this.params.radius = $("[name='postcode-radius']:checked").val();
                     this.params.step = 3;
                  }
                  break;
               case 3:
                  if(this.params.age === null) {
                     this.params.age = $("[name='q3-age']:checked").val();//get default age from html
                     this.params.step = 4;
                     // this.error = "Please select a valid age range.";
                  }else {
                     this.params.step = 4;
                  }
                  break;
               case 4:
                  if(this.params.community === null) {
                     this.error = "Please select at least one community group."
                  }else {
                     // this.params.step = 4;//handle it in result page
                     window.location.href = getSearchUrl();
                  }
                  break;
               default:
                  this.params.step = 1;
                  break;
            }
         },
         onError: function (error) {
            let errMessage = "We're sorry, we're not able to retrieve the data at this moment, please check your internet and try refreshing your browser.";
            if (typeof error.response !== "undefined") {
               errMessage = error.response.data.error;
            }else if(error){
               errMessage = error;
            }
            //make it so if this.message.error is not null we display the error messages
            // this.error = errMessage;
            console.error(errMessage);
         },
         updateStep: function(step) {
            this.params.step = step;
            this.scrollToForm = true;
         },
         resetPostcodeToEmpty: function () {
            this.params.step = 2;
            this.params.postcode = null;
            this.params.lng = null;
            this.params.lat = null;
            this.params.radius = null;
         }
      },
      computed: {},
      watch: {
         params: {
            handler: function(newVal) {
               updateUrlParams(newVal);
               this.searchUrl = getSearchUrl();
               this.countFilteredSupportOptions = filterSupportOptions(newVal, this.supportOptions, this.pathway,true);
               this.error = null;//refresh error when an update is succeed;
            },
            deep: true,
         }
      },
      updated(){
         if(this.scrollToForm){
            scrollToViewOfStep(this.params.step);
            this.scrollToForm = false;
         }
      },
      mounted() {
         //when mounted called, the component stopped mounting.
         this.initialising.mounting = false;

         //load pathway categories
         if(this.pathway){
            axios.get(baseAxiosPath +"get-categories?group=supportCategoryPathway"+ this.pathway)
                .then(response=>{this.categories.supportCategory=response.data})
                .catch(this.onError)
                .finally(()=>{this.initialising.supportCategory = false;});
         }

         //load support options
         axios.get(baseAxiosPath+"get")
             .then(response => {
                this.supportOptions = response.data;
                this.countFilteredSupportOptions = filterSupportOptions(this.params, this.supportOptions, this.pathway,true);
                // console.log(this.supportOptions);
             })
             .catch(this.onError)
             .finally(() => this.initialising.supportOptions = false);

         //load q2
         initAutocomplete();

         //load q3 ages
         axios.get(baseAxiosPath + "get-categories?group=ageGroup")
             .then(response=>this.categories.ageGroup=response.data)
             .catch(this.onError)
             .finally(()=>this.initialising.ageGroup = false);

         //load q4 communities
         axios.get(baseAxiosPath + "get-categories?group=communityGroup")
             .then(response=>this.categories.communityGroup=response.data)
             .catch(this.onError)
             .finally(()=>this.initialising.communityGroup = false);

         scrollToViewOfStep(this.params.step);
      }
   });
});
