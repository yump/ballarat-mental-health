import {
    baseAxiosPath,
    getUrlParam,
    updateUrlParams,
    filterResource,
    capResource
} from './services/resource';
import {capIncrement, scrollToElement, validCap} from "./services/utility";

+$(function () {
    const urlParam = getUrlParam();

    new Vue({
        el: '#resource-landing-vue',
        delimiters: ['{*', '*}'],
        data: {
            params: urlParam,
            categories: {
                resourceCategory : [],
                // resourceUserType: []
            },
            resource: [],
            filteredResource: [],
            cappedResource: [],
            initialising: {
                all: true,
                resource: true,
                update: true,
                resourceCategory: true,
                // resourceUserType: true,
            },
            error: null,
        },
        methods: {
            loadMore: function () {
                if(this.params.cap === null ) this.params.cap = 6;
                if((this.params.cap + capIncrement) < this.filteredResource.length) {
                    this.params.cap += capIncrement;
                } else {
                    this.params.cap = validCap(this.filteredResource.length);
                }
                // this.$el.blur();
                this.$nextTick().then(()=>this.focusNextElement());
            },
            updateResults: function (reinitialiseCap = true, loadingDelay = true) {
                if(reinitialiseCap) this.params.cap = null;
                if(loadingDelay){
                    this.initialising.update = true;
                    //manually set a 0.5 second delay, creating a loading effect
                    setTimeout(()=>{
                        this.filteredResource = filterResource(this.params, this.resource, false);
                        this.cappedResource = capResource(this.params, this.filteredResource);
                        this.initialising.update = false;
                    }, 500);
                }else{
                    this.filteredResource = filterResource(this.params, this.resource, false);
                    this.cappedResource = capResource(this.params, this.filteredResource);
                }
            },
            focusNextElement: function () {
                const vueInstance = this;
                const list = document.getElementById("resource-landing-results-lists");
                // console.log(list);
                if(list){
                    const focusIndex = parseInt(vueInstance.params.cap) - 6;
                    // console.log(focusIndex);
                    // console.log(list.childNodes);
                    // console.log(list.childNodes.length);
                    list.childNodes.forEach(function (e, i) {
                        if(i === focusIndex){
                            // console.log(e, i);
                            const link = e.firstChild;
                            link.focus();
                            scrollToElement(link);
                        // }else{
                        //     console.log(i);
                        }
                    });
                }
            }
        },
        watch: {
            params: {
                handler: function(newVal) {
                    updateUrlParams(newVal);
                    this.updateResults(false);
                    this.error = null;//refresh error when an update is succeed;
                },
                deep: true,
            }
        },
        mounted() {
            //load support options
            axios.get(baseAxiosPath+"get")
                .then(response => {
                    this.resource = response.data;
                    // console.log(this.resource);
                    this.updateResults(false, false);
                    this.initialising.update = false;
                })
                .catch(this.onError)
                .finally(() => this.initialising.resource = false);

            //load resource category
            axios.get(baseAxiosPath + "get-categories?group=resourceCategory")
                .then(response=>this.categories.resourceCategory=response.data)
                .catch(this.onError)
                .finally(()=>this.initialising.resourceCategory = false);

            //load resource category
            // axios.get(baseAxiosPath + "get-categories?group=resourceUserType")
            //     .then(response=>{this.categories.resourceUserType=response.data;console.log(this.categories.resourceUserType);})
            //     .catch(this.onError)
            //     .finally(()=>this.initialising.resourceUserType = false);
        }
    });
});
