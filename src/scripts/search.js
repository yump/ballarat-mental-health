import {
    defaultPaginate,
    baseAxiosPath,
    getUrlParam,
    updateUrlParams,
    filterEntries,
    paginate
} from './services/search.js';
import {scrollToElement} from "./services/utility";

+$(function () {
    const urlParam = getUrlParam();

    new Vue({
        el: '#search-vue',
        delimiters: ['{*', '*}'],
        data: {
            params: urlParam,
            maxPage: 1,
            pageList: [],
            entries: [],
            filteredEntries: [],
            paginateEntries: [],
            initialising: {
                all: true,
                update: true,
                entries: true,
            },
            error: null,
        },
        methods: {
            updateResults: function (reinitialisePage = true, loadingDelay = true) {
                if(reinitialisePage) this.params.page = null;
                if(loadingDelay){
                    this.initialising.update = true;
                    //manually set a 0.5 second delay, creating a loading effect
                    setTimeout(()=>{
                        this.filteredEntries = filterEntries(this.params, this.entries, false);
                        this.maxPage = Math.ceil(this.filteredEntries.length / defaultPaginate);
                        this.paginateEntries = paginate(this.params, this.filteredEntries, false);
                        this.initialising.update = false;
                    }, 500);
                }else{
                    this.filteredEntries = filterEntries(this.params, this.entries, false);
                    this.maxPage = Math.ceil(this.filteredEntries.length / defaultPaginate);
                    this.paginateEntries = paginate(this.params, this.filteredEntries, false);
                }
            },
            switchPage: function (page) {
                // const page = event.target.dataset.value;
                // console.log(page);
                page = page.toString();
                if(page > this.maxPage) {
                    this.params.page = this.maxPage;
                }else{
                    this.params.page = page;
                }
                this.$nextTick(this.focusNextElement());
            },
            nextPage: function(){
                if(parseInt(this.params.page) < parseInt(this.maxPage)){
                    this.params.page = (parseInt(this.params.page!==null?this.params.page:1) + 1).toString();
                }
                this.$nextTick(this.focusNextElement());
            },
            prevPage: function () {
                if(this.params.page !== 1){
                    this.params.page = (parseInt(this.params.page!==null?this.params.page:1) - 1).toString();
                }
                this.$nextTick(this.focusNextElement());
            },
            getPageList: function(currentPage, maxPage){
                if(currentPage === null && this.params.page){
                    currentPage = this.params.page;
                }else if(this.params.page === null){
                    currentPage = 1;
                }

                if(maxPage === null){
                    maxPage = this.maxPage;
                }
                currentPage = parseInt(currentPage);
                maxPage = parseInt(maxPage);

                const pageArr = [];
                const pageListLimit = 2;

                let i = currentPage;
                if(currentPage === maxPage && maxPage > 2){
                    i = i - 1;
                }

                while(pageArr.length < pageListLimit && i <= maxPage){
                    if(i > 0)pageArr.push(i);
                    i++;
                }
                return pageArr;
            },
            focusNextElement: function(){
                scrollToElement("#search-results-list");
                const element = document.getElementById("search-results-list");
                element.childNodes.forEach(function (e,i) {
                    if(i === 0){
                        // console.log(e.firstChild);
                        e.firstChild.focus();
                    }
                })
            }
        },
        watch: {
            params: {
                handler: function(newVal) {
                    updateUrlParams(newVal);
                    this.updateResults(false);
                    this.pageList = this.getPageList(newVal.page, null);
                    this.error = null;//refresh error when an update is succeed;
                },
                deep: true,
            },
            maxPage: {
                handler: function (newVal) {
                    this.pageList = this.getPageList(null, newVal);
                }
            }
        },
        mounted() {
            //load support options
            // console.log(this.params.q);
            axios.get(baseAxiosPath+"get?q="+this.params.q)
                .then(response => {
                    this.entries = response.data;
                    // console.log(this.entries);
                    this.updateResults(false, false);
                    this.initialising.update = false;
                    if(this.params.page === null){
                        this.params.page = "1";
                    }
                    this.pageList = this.getPageList(this.params.page, this.maxPage);
                    // console.log(response.data);
                })
                .catch(this.onError)
                .finally(() => this.initialising.entries = false);
        }
    });
});
