import { alertSpacer, initAlert, closeAlert } from '../services/alert';
const mobileMenu = $('#navbarSupportedContent');

+$(function (){
    $('[data-alert]').each(function () {
        initAlert($(this).attr('data-alert'));
    });

    $('[data-alert-close]').on('click', function (e) {
        // Do not perform default action when button is clicked
        e.preventDefault();
        closeAlert($(this).attr('data-alert-close'));
        alertSpacer.css({display: 'none'}).hide();
    });

    mobileMenu.on('shown.bs.collapse', function (e){
        $('.site-wide-alert').addClass(function (e){
           $(this).addClass('d-none');
        });
        $('.header__spacer--alert').each(function (e){
           $(this).addClass("d-none");
        });
    });

    mobileMenu.on('hidden.bs.collapse', function (e){
        $('.site-wide-alert').each(function (e){
            $(this).removeClass("d-none");
        });
        $('.header__spacer--alert').each(function (e){
            $(this).removeClass("d-none");
        });
    });
});
