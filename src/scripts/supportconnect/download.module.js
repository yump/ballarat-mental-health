+$(function () {
   $('[data-action="click-child-button"]').each(function () {
      $(this).on('click', function (){
          const parent = $(this);
          parent.find('[data-action="child-button"]').each(function(){
              const childButton = $(this);
              childButton[0].click();
          });
      });
   });
});
