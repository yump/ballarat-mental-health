+$(function () {
    $('[data-action="quick-exit"]').on("click", function (e) {
        $("body").hide();
        // Get away right now
        window.open("https://google.com.au", "_blank");
        // Replace current site with another benign site
        window.location.replace('http://google.com.au');
    });
});
