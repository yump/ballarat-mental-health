//jquery is already imported by other module, don't import it again here.
import {updateSpacerHeight} from '../services/alert';

//shrink the nav-bar when scroll down
$(document).on('scroll', function () {
    if ($(document).scrollTop() > 100) {
        $('header').addClass('header--shrink');
		$('body').addClass('header-shrunk');
    } else {
        $('header').removeClass('header--shrink');
		$('body').removeClass('header-shrunk');
        updateSpacerHeight();
    }
});
