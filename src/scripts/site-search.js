var initShowingItems = 12;
var increment = 12;

// make 'currentShowing' always (initShowingItems + increment * x), to avoid showing weird number of items with load more button
function validateCurrentShowing(rawNumber) {
  // my weird math stuff...
  var m = (rawNumber - initShowingItems) / increment;
  var x = isInt(m) ? m : Math.floor(m + 1);
  var validatedVal = increment * x + initShowingItems;
  // console.log(validatedVal);
  return validatedVal;
}

function isInt(value) {
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
}

$(function() {
	let searchVue = new Vue({
		el: '#site-search-results',
		delimiters: ['{*', '*}'],
		data: {
			allResults: null,
			initErrored: false,
			message: {
				info: null,
				notice: null,
				error: null,
			},
			initialising: true,
			filters: {
				pages: window.urlParameters.pages == -1 ? false : true,
				news: window.urlParameters.news == -1 ? false : true,
				documents: window.urlParameters.documents == -1 ? false : true,
			},

			currentShowing: window.urlParameters.showing && validateCurrentShowing(window.urlParameters.showing) ? validateCurrentShowing(window.urlParameters.showing) : initShowingItems,
	  	},
	  	methods: {
	  		loadMore: function() {
				if((this.currentShowing + increment) < this.filteredResults.length) {
					this.currentShowing = this.currentShowing + increment;
				} else {
					this.currentShowing = validateCurrentShowing(this.filteredResults.length);
				}
			},
	  	},
	  	computed: {
	  		filteredResults: function() {
	  			var filteredResults = [];
	  			if(this.allResults && this.allResults.length) {
	  				for (var i = 0; i < this.allResults.length; i++) {
	  					var item = this.allResults[i];

	  					if(this.filters[item.filterKey]) {
	  						filteredResults.push(item);
	  					}
	  					
	  				}
	  			}
	  			return filteredResults;
	  		},
	  		sortedFilteredResults: function() {
	  			if(this.filteredResults) {
	  				var cloneResults = this.filteredResults.slice(0);
	  				cloneResults.sort(function(p1, p2) {
		  				if(p1.score < p2.score) {
		  					return 1;
		  				}
		  				if(p1.score > p2.score) {
		  					return -1;
		  				}
		  				return 0;
		  			});
		  			return cloneResults;
	  			}
	  			return [];
	  		},
	  		cappedSortedFilteredResults: function() {
	  			if(this.sortedFilteredResults.length > 0) {
	  				return this.sortedFilteredResults.slice(0, this.currentShowing); // if currentShowing is larger than the length, will return the full filteredSpecialists array instead of an error
	  			}
	  			return [];
	  		},
	  		showLoadMore: function() {
				if(this.filteredResults.length > 0) {
					if(this.currentShowing < this.filteredResults.length) {
						return true;
					}
				}
				return false;
			}, 
	  	},
	  	watch: {
	  		filters: {
	  			handler: function(newVal) {
	  				updateUrlParams(newVal, this.currentShowing);
	  			},
	  			deep: true,
	  		},
	  		currentShowing: function(newVal) {
	  			updateUrlParams(this.filters, newVal);
	  		}
	  	},
	  	mounted() {
        	axios
            .get('/actions/yump-module/default/search?q=' + window.urlParameters.q)
            .then(response => {
                this.allResults = response.data;

                // console.log(response.data);

            })
            .catch(error => {
                let errMessage = "We're sorry, we're not able to retrieve the data at this moment, please check your internet and try refreshing your browser.";
                if(error.response.data.error) {
                	errMessage = error.response.data.error;
                }
                this.initErrored = true;
                this.message.error = errMessage;
            })
            .finally(() => this.initialising = false)
    	},
	});
});

function updateUrlParams(filters, currentShowing) {
	if(!$('html').hasClass('ieLessThan10')) { // otherwise we will hit js error and break other scripts
		var paramString = objToString({
			q: window.urlParameters.q,
			pages: filters.pages ? null : -1,
			news: filters.news ? null : -1,
			documents: filters.documents ? null : -1,
			showing: currentShowing && currentShowing > 12 ? currentShowing : null,
		});

		var newUrl = window.location.origin + window.location.pathname + '?' + paramString;
		history.replaceState(null, null, newUrl); // Doesn't work for IE 10 and less, might need to change this into window.location.hash
	}
}

function objToString (obj) {
    var str = '';
    for (var p in obj) {
        if (obj.hasOwnProperty(p) && obj[p]) {
            str += p + '=' + encodeURIComponent(obj[p]) + '&'; // need to encode the value, cuz users might put in white spaces, etc
        }
    }
    if (str.substr(str.length - 1) == '&') {
      str = str.substr(0, str.length - 1);
    }
    return str;
}
