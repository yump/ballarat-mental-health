const alertSpacer = $('.header__spacer--alert');
const alertContainer = $('.site-wide-alert');
let alertID;

const initAlert = (alertId) => {
    setAlertId(alertId);
    // Check if alert has been closed
    if (getCookie('alert-' + alertID) !== 'closed') {
        const alertBlock = jQuery('[data-alert="' + alertID + '"]');
        alertBlock.css({display: 'flex'}).show();
        alertSpacer.css({display: 'flex'}).show();

        updateSpacerHeight();
        //update height when resize
        window.addEventListener('resize', function () {
            updateSpacerHeight();
        });
    }else {
        window.alertHeight = 0;
    }
};

const updateSpacerHeight = () => {
    const alertHeight = alertContainer.outerHeight();
    window.alertHeight = alertHeight;
    alertSpacer.css({
        'padding-top': alertHeight
    });
};

const setAlertId = (alertId) => {
    alertID = alertId;
};

const closeAlert = (alertID) => {
    setCookie('alert-' + alertID, 'closed', 2);
    $('[data-alert="' + alertID + '"]').slideUp();
};

const setCookie = (key, value, expiry) => {
    const expires = new Date();
    expires.setTime(expires.getTime() + expiry * 24 * 60 * 60 * 1000);
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
};

const getCookie = (key) => {
    const keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null
};

const eraseCookie = (key) => {
    const keyValue = getCookie(key);
    setCookie(key, keyValue, '-1');
};

export {
    alertContainer,
    alertSpacer,
    setAlertId,
    updateSpacerHeight,
    initAlert,
    closeAlert,
}
