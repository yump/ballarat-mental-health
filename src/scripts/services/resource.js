import {objectToString, validCap, initCap} from "./utility";

const baseAxiosPath = "/actions/support-connect/resource-library/";

const getUrlParam = () => {
    try {
        const params = (new URL(document.location)).searchParams;
        //params.get() returns string|null
        return {
            cap: validCap(params.get("cap")),
            categories: params.get("categories")?params.get("categories"):null,
            // userType: params.get("userType")?params.get("userType"):null,
            search: params.get("search")?params.get("search"):null,
        };
    } catch (e) {
        console.error(e);
        return {
            cap: null,
            categories: null,
            // userType: null,
            search: null,
        };
    }
};

const updateUrlParams = (params) => {
    if(!$('html').hasClass('ieLessThan10')) {
        const paramString = objectToString({
            cap: params.cap && params.cap > initCap ? params.cap : null,
            // userType: params.userType? params.userType : null,
            categories: params.categories? params.categories : null,
            search: params.search? params.search : null,
        });

        let newUrl = window.location.origin + window.location.pathname + '?' + paramString;
        history.replaceState(null, null, newUrl); //Doesn't work for IE 10 and less, might need to change this into window.location.hash
        window.dispatchEvent(new Event('popstate'));//let others know url updated
    }
};

const filterResource = (params, resource) => {
    //filter resource categories
    if(params.categories){
        resource = resource.filter(e=>{
            return e.resourceCategory.includes(params.categories);
        });
    }

    //filter resource categories
    // if(params.userType){
    //     resource = resource.filter(e=>{
    //         return e.resourceUserType.includes(params.userType);
    //     });
    // }

    if(params.search){
        const trimmedLowerKeyword = params.search.trim().toLowerCase();
        resource = resource.filter(e=>{
            return !trimmedLowerKeyword
                || trimmedLowerKeyword === ""
                || e.title.toLowerCase().indexOf(trimmedLowerKeyword) !== -1
                || e.summary.toLowerCase().indexOf(trimmedLowerKeyword) !== -1
                || e.tagsCombinedString.toLowerCase().indexOf(trimmedLowerKeyword) !== -1
        })
    }

    return resource;
};

const capResource = (params, filteredResource) => {
    const cap = params.cap ? parseInt(params.cap) : initCap;
    return filteredResource.filter((e,i)=>{
        return i < cap;
    });
};

export {
    baseAxiosPath,
    getUrlParam,
    updateUrlParams,
    filterResource,
    capResource
}
