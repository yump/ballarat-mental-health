// +$(function () {
//     initSidebar();
// });

// const initSidebar = () =>{
//     const main = $("#main");
//     const callBack = $("#sidebar-callback");

//     setSidebar(callBack, main);
//     $(document).on('scroll', function () {
//         setSidebar(callBack, main);
//     });
// };

// const setSidebar = (callBack, main) =>{
//     const defaultMarginBottom = 60 + 106; //106 is a buffer
//     const bufferBegin = 16;

//     const alertHeight = window.alertHeight ? window.alertHeight : 0;
//     const fixBegin = $("#nav-main").outerHeight() + $("#nav-secondary").outerHeight() - bufferBegin; //just need nav-bar's height, not interest in alert's height
//     const fixEnd = main.outerHeight() + main.offset().top - callBack.outerHeight() - defaultMarginBottom - alertHeight - bufferBegin;

//     const scrollTop = $(document).scrollTop();

//     const floatTop = fixBegin + alertHeight;
//     // console.log(main.outerHeight());
//     // console.log("alert height", alertHeight);
//     // console.log("floatTop", floatTop);
//     // console.log("fixBegin", fixBegin);
//     // console.log("scrollTop",scrollTop);
//     // console.log("fixEnd",fixEnd);

//     if (scrollTop > fixBegin && scrollTop < fixEnd) {
//         // console.log('in');
//         callBack
//             .addClass('sidebar-callback__container--fixed')
//             .css({"margin-top": 0, "top": floatTop.toString() + "px"});
//             // .css({"top": floatTop.toString() + "px"});
//     } else if(scrollTop > fixEnd) {
//         // console.log('bottom');
//         callBack
//             .removeClass('sidebar-callback__container--fixed')
//             .css({"margin-top": 'auto', "top": "0"});
//             // .css({"padding-top": 'auto'});
//     } else {
//         // console.log('above');
//         callBack
//             .removeClass('sidebar-callback__container--fixed')
//             .css({"margin-top": 0, "top": "0"});
//             // .css({"top": 0});
//     }
// };

// export {
//     initSidebar
// }
