import {objectToString} from "./utility";

const baseAxiosPath = "/actions/support-connect/search/";
const defaultPaginate = 7;

const getUrlParam = () => {
    try {
        const params = (new URL(document.location)).searchParams;
        //params.get() returns string|null
        return {
            q: params.get("q")?params.get("q"):null,
            page: params.get("page")?params.get("page"):null,
            support: params.get("support")?params.get("support"):null,
            resource: params.get("resource")?params.get("resource"):null,
            others: params.get("others")?params.get("others"):null,
        };
    } catch (e) {
        console.error(e);
        return {
            q: null,
            page: null,
            support: params.service?params.support:null,
            resource: params.resource?params.resource:null,
            others: params.others?params.others:null,
        };
    }
};

const updateUrlParams = (params) => {
    if(!$('html').hasClass('ieLessThan10')) {
        const paramString = objectToString({
            q: params.q? params.q : null,
            page: params.page? params.page:null,
            support: params.support?params.support:null,
            resource: params.resource?params.resource:null,
            others: params.others?params.others:null,
        });

        let newUrl = window.location.origin + window.location.pathname + '?' + paramString;
        history.replaceState(null, null, newUrl); //Doesn't work for IE 10 and less, might need to change this into window.location.hash
        window.dispatchEvent(new Event('popstate'));//let others know url updated
    }
};

const filterEntries = (params, entries) => {
    // //toggle when search
    // if(params.q){
    //     const trimmedLowerKeyword = params.q.trim().toLowerCase();
    //     entries = entries.filter(e=>{
    //         return !trimmedLowerKeyword
    //             || trimmedLowerKeyword === ""
    //             || e.title.toLowerCase().indexOf(trimmedLowerKeyword) !== -1
    //             || e.summary.toLowerCase().indexOf(trimmedLowerKeyword) !== -1
    //             || e.tagsCombinedString.toLowerCase().indexOf(trimmedLowerKeyword) !== -1
    //     })
    // }

    //toggle support, resource, others
    if(params.support || params.resource || params.others){
        entries = entries.filter(e=>{
            return (params.support && e.type === "support")
                || (params.resource && e.type === "resource")
                || (params.others && e.type === "others");
        })
    }

    //paginate
    entries.map((e, i)=>{
        e.page = Math.ceil((i+1) /  defaultPaginate).toString();
    });

    // console.log(entries);

    return entries;
};

const paginate = (params, filteredEntries) => {
    const pageNum = params.page ? params.page: '1';

    return filteredEntries.filter(e=>{
        return e.page === pageNum;
    });
};

export {
    baseAxiosPath,
    defaultPaginate,
    getUrlParam,
    updateUrlParams,
    filterEntries,
    paginate
}
