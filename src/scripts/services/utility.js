const initCap = 6;
const capIncrement = 6;

const isInt = (value) => {
    return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
};

const objectToString = (obj) => {
    let str = '';
    for (let p in obj) {
        if (obj.hasOwnProperty(p) && obj[p]) {
            let v = encodeURIComponent(obj[p]);
            if(Array.isArray(obj[p])){
                v = encodeURIComponent(obj[p].join("_"))
            }
            str += p + '=' + v + '&'; // need to encode the value, cuz users might put in white spaces, etc
        }
    }
    if (str.substr(str.length - 1) === '&') {
        str = str.substr(0, str.length - 1);
    }
    return str;
};

const coordinatesDistance = (lat1, lng1, lat2, lng2) =>{
    const R = 6371e3; // metres
    const radians1 = lat1 * Math.PI/180; // φ, λ in radians
    const radians2 = lat2 * Math.PI/180;
    const latRadians = (lat2-lat1) * Math.PI/180;
    const lngRadians = (lng2-lng1) * Math.PI/180;
    const a = Math.sin(latRadians/2) * Math.sin(latRadians/2) +
        Math.cos(radians1) * Math.cos(radians2) *
        Math.sin(lngRadians/2) * Math.sin(lngRadians/2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    return R * c; // in metres
};

const validCap = (cap) => {
    if(!cap){
        return initCap;
    }
    let m = (cap - initCap) / capIncrement;
    let x = isInt(m)?m : Math.floor(m + 1);
    return capIncrement * x + initCap;
};

const scrollToElement = (elementId) => {
    const pos = $(elementId).offset().top - 230;
    $('html, body').animate({
        scrollTop: pos
    }, 800);
};

export {
    initCap,
    capIncrement,
    validCap,
    isInt,
    objectToString,
    coordinatesDistance,
    scrollToElement
}
