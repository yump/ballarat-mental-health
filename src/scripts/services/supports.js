import {initCap, validCap, objectToString, coordinatesDistance} from "./utility";

const baseAxiosPath = "/actions/support-connect/support-finder/";
const catchmentPostCode = [
    '3064',
    '3074',
    '3075',
    '3076',
    '3079',
    '3081',
    '3082',
    '3083',
    '3084',
    '3085',
    '3087',
    '3088',
    '3089',
    '3090',
    '3091',
    '3094',
    '3095',
    '3096',
    '3097',
    '3099',
    '3101',
    '3102',
    '3103',
    '3104',
    '3105',
    '3106',
    '3107',
    '3108',
    '3109',
    '3109',
    '3111',
    '3113',
    '3114',
    '3115',
    '3116',
    '3122',
    '3123',
    '3124',
    '3125',
    '3126',
    '3127',
    '3128',
    '3129',
    '3130',
    '3131',
    '3132',
    '3133',
    '3134',
    '3135',
    '3136',
    '3137',
    '3138',
    '3139',
    '3140',
    '3146',
    '3147',
    '3148',
    '3149',
    '3150',
    '3151',
    '3152',
    '3153',
    '3154',
    '3155',
    '3156',
    '3158',
    '3159',
    '3160',
    '3166',
    '3167',
    '3168',
    '3170',
    '3178',
    '3179',
    '3180',
    '3658',
    '3750',
    '3751',
    '3752',
    '3753',
    '3754',
    '3755',
    '3756',
    '3757',
    '3758',
    '3759',
    '3760',
    '3761',
    '3762',
    '3763',
    '3765',
    '3766',
    '3767',
    '3770',
    '3775',
    '3777',
    '3782',
    '3785',
    '3786',
    '3787',
    '3788',
    '3789',
    '3791',
    '3792',
    '3793',
    '3795',
    '3796',
    '3797',
    '3799',
    '3800',
    '3804',
];

const getUrlParam = () => {
    try {
        const params = (new URL(document.location)).searchParams;
        //params.get() returns string|null
        return {
            cap: validCap(params.get("cap")),
            role: params.get("role"),
            categories: params.get("categories")?params.get("categories").split("_"):[],
            step: params.get('step')?parseInt(params.get('step')):1,
            postcode: params.get('postcode')? params.get('postcode'):null,
            radius:params.get('radius')?params.get('radius'):null,
            lat:params.get('lat')?params.get('lat'):null,
            lng:params.get('lng')?params.get('lng'):null,
            age:params.get('age')?params.get('age'):null,
            community: params.get("community")?params.get("community").split("_"):[],
            delivery: params.get("delivery")?params.get("delivery").split("_"):[],
            referral: params.get("referral")?params.get("referral").split("_"):[],
            fees: params.get("fees")?params.get("fees").split("_"):[],
            // speciality: params.get("speciality")?params.get("speciality").split("_"):[],
        };
    }catch (e) {
        console.error(e);
        return {
            cap: null,
            role: null,
            categories: [],
            step: 1,
            postcode: null,
            radius: null,
            lat: null,
            lng: null,
            age: null,
            community: [],
            delivery: [],
            referral: [],
            fees: [],
            // speciality: [],
        };
    }
};

const updateUrlParams = (params) => {
    if(!$('html').hasClass('ieLessThan10')) {
        const paramString = objectToString({
            cap: params.cap && params.cap > initCap ? params.cap : null,
            role: params.role ? params.role :null,
            categories: params.categories.length !== 0 ? params.categories: null,
            step:params.step ? params.step: null,
            postcode: params.postcode? params.postcode: null,
            radius: params.radius?params.radius:null,
            lat: params.lat?params.lat:null,
            lng: params.lng?params.lng:null,
            age: params.age?params.age:null,
            community: params.community.length !== 0 ? params.community: null,
            delivery: params.delivery.length !== 0 ? params.delivery: null,
            referral: params.referral.length !== 0 ? params.referral: null,
            fees: params.fees.length !== 0 ? params.fees: null,
            // speciality: params.speciality.length !== 0 ? params.speciality: null,
        });

        let newUrl = window.location.origin + window.location.pathname + '?' + paramString;
        history.replaceState(null, null, newUrl); //Doesn't work for IE 10 and less, might need to change this into window.location.hash
        window.dispatchEvent(new Event('popstate'));//let others know url updated
    }
};

const getPathway = () => {
    const pathname = window.location.pathname;
    const pathnameArr = pathname.split("/");
    const pathway = pathnameArr.filter(function (e) {
        return e.indexOf('pathway') !== -1;
    })[0];

    //"pathway-1" get "1"
    return pathway.split("-")[1];
};

const filterSupportOptions = (params, supportOptions, pathway, justCount = false) => {
    let result = supportOptions;
    //filter pathway 1,2,3 support categories
    if(params.categories.length !== 0){
        result = result.filter(e=>{
            switch (pathway) {
                case '1':
                    return params.categories.filter(ce=> e.pathway1.includes(ce) ).length !== 0;
                case '2':
                    return params.categories.filter(ce=> e.pathway2.includes(ce) ).length !== 0;
                case '3':
                    return params.categories.filter(ce=> e.pathway3.includes(ce) ).length !== 0;
                default:
                    return params.categories.filter(ce=> e.pathway1.includes(ce) ).length !== 0;
            }
        });
    }

    //calculate within distance
    if(params.radius && params.lat && params.lng){
        result = result.filter(e=>{
            if(typeof e.ignoreRadius !== "undefined" && e.ignoreRadius === true){
                return true;
            }
            return coordinatesDistance(e.lat, e.lng, params.lat, params.lng) < params.radius * 1000;
        });
    }

    //filter age
    if(params.age){
        result = result.filter(e=>{
           return e.ageGroup.includes(params.age);
        });
    }

    //filter community
    if(params.community.length !== 0) {
        result = result.filter(e=>{
            return params.community.filter(ce => e.communityGroup.includes(ce)).length !== 0;
        });
    }

    if(justCount){
        return result.length;
    }

    //result page has few more filters

    //filter referral
    if(params.referral.length !== 0) {
        result = result.filter(e=>{
            return params.referral.filter(ce => e.referralType.includes(ce)).length !== 0;
        });
    }

    //filter delivery methods
    if(params.delivery.length !== 0) {
        result = result.filter(e=>{
            return params.delivery.filter(ce => e.deliveryMethod.includes(ce)).length !== 0;
        });
    }

    //filter fees
    if(params.fees.length !== 0) {
        result = result.filter(e=>{
            return params.fees.filter(ce => e.fees.includes(ce)).length !== 0;
        });
    }

    //filter speciality
    // if(params.speciality.length !== 0) {
    //     result = result.filter(e=>{
    //         return params.speciality.filter(ce => e.speciality.includes(ce)).length !== 0;
    //     });
    // }

    return result;
};

/**
 *
 * @param params
 * @param filteredSupportOptions
 * @returns Array {*}
 */
const capSupportOptions = (params, filteredSupportOptions) => {
    const cap = params.cap ? parseInt(params.cap) : initCap;
    return filteredSupportOptions.filter((e,i)=>{
        return i < cap;
    });
};

export {
    baseAxiosPath,
    catchmentPostCode,
    validCap,
    getUrlParam,
    updateUrlParams,
    getPathway,
    filterSupportOptions,
    capSupportOptions
}
