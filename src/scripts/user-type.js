+$(function () {
    const userTypeCardsSelectKeywords = '[data-action="global-user-type-card"]';
    const userTypeCards = $('[data-action="global-user-type-card"]');

    userTypeCards.each(function () {
        $(this).click(function () {
            const userTypeCard = $(this);
            //click on a already inactive tab
            if(userTypeCard.hasClass("inactive")){

                //apply inactive to active siblings
                userTypeCard.siblings(userTypeCardsSelectKeywords).each(function(){
                    const userTypeCardSiblings = $(this);
                    if(userTypeCardSiblings.hasClass("active")){
                        userTypeCardSiblings.removeClass("active").addClass("inactive");
                    }
                    hideOrShowTarget(userTypeCardSiblings, true);
                });

                //apply active to clicked userTypeCard
                userTypeCard.removeClass("inactive").addClass("active");
                hideOrShowTarget(userTypeCard, false);

                //first time click on a tab
            }else if(!userTypeCard.hasClass("active") && !userTypeCard.hasClass("inactive")){
                userTypeCard.addClass("active");
                userTypeCards.each(function(){
                    if(!$(this).hasClass("active")){
                        $(this).addClass("inactive");
                        hideOrShowTarget($(this), true);
                    }
                });
                hideOrShowTarget(userTypeCard, false);

                //clicked on active tab again, revert to initial state
            }else if(userTypeCard.hasClass("active") && !userTypeCard.hasClass("inactive")){
                userTypeCard.removeClass("active");
                userTypeCards.each(function () {
                    if($(this).hasClass("inactive")){
                        $(this).removeClass("inactive");
                    }
                });
                hideOrShowTarget(userTypeCard, true);
            }
        });
    });
});

const hideOrShowTarget = (element, isHide=true) =>{
    const targetLink = element.attr("data-target"),
        targetEl = '[data-parent="'+ targetLink + '"]';

    element.attr("aria-expanded", !isHide);
    $(targetEl).each(function (){
        const targetLinkElement = $(this);
        if(isHide){
            if(!targetLinkElement.hasClass("d-none")){
                targetLinkElement.addClass("d-none");
            }
        }else if(!isHide){
            if(targetLinkElement.hasClass("d-none")){
                targetLinkElement.removeClass("d-none");
            }
        }
    });
};
