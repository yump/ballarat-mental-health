import {catchmentPostCode} from "./services/supports";
const postCodeActiveClass = "card--postcode-validating-card-active";
const validateButton = $("[data-action='validate-postcode']");
const restartButton = $("[data-action='restart-postcode']");
const autocomplete = $("#autocomplete");

const postcodeCard1 = $("#postcode-card-1");
const postcodeCard2 = $("#postcode-card-2");
const postcodeCard3 = $("#postcode-card-3");


+$(function () {
    validateButton.click(function () {
        if(catchmentPostCode.includes(autocomplete.val())){
            showValidPostcode(autocomplete.val(), lat, lng);
        }else{
            showInvalidPostcode(postcode);
        }
    });

    restartButton.click(()=>{
        $("#autocomplete").val();
        postcodeCard3.removeClass(postCodeActiveClass);
        postcodeCard1.addClass(postCodeActiveClass);
    });
});

const showValidPostcode = (postcode, lat, lng) => {
    $("#covered-postcode").html(postcode);
    $("#valid-postcode").val(postcode);
    $("#valid-lat").val(lat);
    $("#valid-lng").val(lng);

    postcodeCard1.removeClass(postCodeActiveClass);
    postcodeCard2.addClass(postCodeActiveClass);
};

const showInvalidPostcode = (postcode) => {
    $("#outside-postcode").html(postcode);
    postcodeCard1.removeClass(postCodeActiveClass);
    postcodeCard3.addClass(postCodeActiveClass);
};
