/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/scripts/user-type.js":
/*!**********************************!*\
  !*** ./src/scripts/user-type.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n+$(function () {\n    var userTypeCardsSelectKeywords = '[data-action=\"global-user-type-card\"]';\n    var userTypeCards = $('[data-action=\"global-user-type-card\"]');\n\n    userTypeCards.each(function () {\n        $(this).click(function () {\n            var userTypeCard = $(this);\n            //click on a already inactive tab\n            if (userTypeCard.hasClass(\"inactive\")) {\n\n                //apply inactive to active siblings\n                userTypeCard.siblings(userTypeCardsSelectKeywords).each(function () {\n                    var userTypeCardSiblings = $(this);\n                    if (userTypeCardSiblings.hasClass(\"active\")) {\n                        userTypeCardSiblings.removeClass(\"active\").addClass(\"inactive\");\n                    }\n                    hideOrShowTarget(userTypeCardSiblings, true);\n                });\n\n                //apply active to clicked userTypeCard\n                userTypeCard.removeClass(\"inactive\").addClass(\"active\");\n                hideOrShowTarget(userTypeCard, false);\n\n                //first time click on a tab\n            } else if (!userTypeCard.hasClass(\"active\") && !userTypeCard.hasClass(\"inactive\")) {\n                userTypeCard.addClass(\"active\");\n                userTypeCards.each(function () {\n                    if (!$(this).hasClass(\"active\")) {\n                        $(this).addClass(\"inactive\");\n                        hideOrShowTarget($(this), true);\n                    }\n                });\n                hideOrShowTarget(userTypeCard, false);\n\n                //clicked on active tab again, revert to initial state\n            } else if (userTypeCard.hasClass(\"active\") && !userTypeCard.hasClass(\"inactive\")) {\n                userTypeCard.removeClass(\"active\");\n                userTypeCards.each(function () {\n                    if ($(this).hasClass(\"inactive\")) {\n                        $(this).removeClass(\"inactive\");\n                    }\n                });\n                hideOrShowTarget(userTypeCard, true);\n            }\n        });\n    });\n});\n\nvar hideOrShowTarget = function hideOrShowTarget(element) {\n    var isHide = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;\n\n    var targetLink = element.attr(\"data-target\"),\n        targetEl = '[data-parent=\"' + targetLink + '\"]';\n\n    element.attr(\"aria-expanded\", !isHide);\n    $(targetEl).each(function () {\n        var targetLinkElement = $(this);\n        if (isHide) {\n            if (!targetLinkElement.hasClass(\"d-none\")) {\n                targetLinkElement.addClass(\"d-none\");\n            }\n        } else if (!isHide) {\n            if (targetLinkElement.hasClass(\"d-none\")) {\n                targetLinkElement.removeClass(\"d-none\");\n            }\n        }\n    });\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyaXB0cy91c2VyLXR5cGUuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2NyaXB0cy91c2VyLXR5cGUuanM/ZTY1NSJdLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbiskKGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdXNlclR5cGVDYXJkc1NlbGVjdEtleXdvcmRzID0gJ1tkYXRhLWFjdGlvbj1cImdsb2JhbC11c2VyLXR5cGUtY2FyZFwiXSc7XG4gICAgdmFyIHVzZXJUeXBlQ2FyZHMgPSAkKCdbZGF0YS1hY3Rpb249XCJnbG9iYWwtdXNlci10eXBlLWNhcmRcIl0nKTtcblxuICAgIHVzZXJUeXBlQ2FyZHMuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICQodGhpcykuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIHVzZXJUeXBlQ2FyZCA9ICQodGhpcyk7XG4gICAgICAgICAgICAvL2NsaWNrIG9uIGEgYWxyZWFkeSBpbmFjdGl2ZSB0YWJcbiAgICAgICAgICAgIGlmICh1c2VyVHlwZUNhcmQuaGFzQ2xhc3MoXCJpbmFjdGl2ZVwiKSkge1xuXG4gICAgICAgICAgICAgICAgLy9hcHBseSBpbmFjdGl2ZSB0byBhY3RpdmUgc2libGluZ3NcbiAgICAgICAgICAgICAgICB1c2VyVHlwZUNhcmQuc2libGluZ3ModXNlclR5cGVDYXJkc1NlbGVjdEtleXdvcmRzKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHVzZXJUeXBlQ2FyZFNpYmxpbmdzID0gJCh0aGlzKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHVzZXJUeXBlQ2FyZFNpYmxpbmdzLmhhc0NsYXNzKFwiYWN0aXZlXCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB1c2VyVHlwZUNhcmRTaWJsaW5ncy5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKS5hZGRDbGFzcyhcImluYWN0aXZlXCIpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGhpZGVPclNob3dUYXJnZXQodXNlclR5cGVDYXJkU2libGluZ3MsIHRydWUpO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgLy9hcHBseSBhY3RpdmUgdG8gY2xpY2tlZCB1c2VyVHlwZUNhcmRcbiAgICAgICAgICAgICAgICB1c2VyVHlwZUNhcmQucmVtb3ZlQ2xhc3MoXCJpbmFjdGl2ZVwiKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgICAgICBoaWRlT3JTaG93VGFyZ2V0KHVzZXJUeXBlQ2FyZCwgZmFsc2UpO1xuXG4gICAgICAgICAgICAgICAgLy9maXJzdCB0aW1lIGNsaWNrIG9uIGEgdGFiXG4gICAgICAgICAgICB9IGVsc2UgaWYgKCF1c2VyVHlwZUNhcmQuaGFzQ2xhc3MoXCJhY3RpdmVcIikgJiYgIXVzZXJUeXBlQ2FyZC5oYXNDbGFzcyhcImluYWN0aXZlXCIpKSB7XG4gICAgICAgICAgICAgICAgdXNlclR5cGVDYXJkLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICAgICAgICAgIHVzZXJUeXBlQ2FyZHMuZWFjaChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghJCh0aGlzKS5oYXNDbGFzcyhcImFjdGl2ZVwiKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJCh0aGlzKS5hZGRDbGFzcyhcImluYWN0aXZlXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaGlkZU9yU2hvd1RhcmdldCgkKHRoaXMpLCB0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIGhpZGVPclNob3dUYXJnZXQodXNlclR5cGVDYXJkLCBmYWxzZSk7XG5cbiAgICAgICAgICAgICAgICAvL2NsaWNrZWQgb24gYWN0aXZlIHRhYiBhZ2FpbiwgcmV2ZXJ0IHRvIGluaXRpYWwgc3RhdGVcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodXNlclR5cGVDYXJkLmhhc0NsYXNzKFwiYWN0aXZlXCIpICYmICF1c2VyVHlwZUNhcmQuaGFzQ2xhc3MoXCJpbmFjdGl2ZVwiKSkge1xuICAgICAgICAgICAgICAgIHVzZXJUeXBlQ2FyZC5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgICAgICAgICB1c2VyVHlwZUNhcmRzLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoJCh0aGlzKS5oYXNDbGFzcyhcImluYWN0aXZlXCIpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKFwiaW5hY3RpdmVcIik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBoaWRlT3JTaG93VGFyZ2V0KHVzZXJUeXBlQ2FyZCwgdHJ1ZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0pO1xufSk7XG5cbnZhciBoaWRlT3JTaG93VGFyZ2V0ID0gZnVuY3Rpb24gaGlkZU9yU2hvd1RhcmdldChlbGVtZW50KSB7XG4gICAgdmFyIGlzSGlkZSA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogdHJ1ZTtcblxuICAgIHZhciB0YXJnZXRMaW5rID0gZWxlbWVudC5hdHRyKFwiZGF0YS10YXJnZXRcIiksXG4gICAgICAgIHRhcmdldEVsID0gJ1tkYXRhLXBhcmVudD1cIicgKyB0YXJnZXRMaW5rICsgJ1wiXSc7XG5cbiAgICBlbGVtZW50LmF0dHIoXCJhcmlhLWV4cGFuZGVkXCIsICFpc0hpZGUpO1xuICAgICQodGFyZ2V0RWwpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgdGFyZ2V0TGlua0VsZW1lbnQgPSAkKHRoaXMpO1xuICAgICAgICBpZiAoaXNIaWRlKSB7XG4gICAgICAgICAgICBpZiAoIXRhcmdldExpbmtFbGVtZW50Lmhhc0NsYXNzKFwiZC1ub25lXCIpKSB7XG4gICAgICAgICAgICAgICAgdGFyZ2V0TGlua0VsZW1lbnQuYWRkQ2xhc3MoXCJkLW5vbmVcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSBpZiAoIWlzSGlkZSkge1xuICAgICAgICAgICAgaWYgKHRhcmdldExpbmtFbGVtZW50Lmhhc0NsYXNzKFwiZC1ub25lXCIpKSB7XG4gICAgICAgICAgICAgICAgdGFyZ2V0TGlua0VsZW1lbnQucmVtb3ZlQ2xhc3MoXCJkLW5vbmVcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9KTtcbn07Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/scripts/user-type.js\n");

/***/ }),

/***/ 8:
/*!****************************************!*\
  !*** multi ./src/scripts/user-type.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/xiaotianhuang/Sites/localhost/_yump/wvphn/src/scripts/user-type.js */"./src/scripts/user-type.js");


/***/ })

/******/ });