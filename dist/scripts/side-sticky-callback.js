/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 11);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/scripts/services/side-sticky-callback.js":
/*!******************************************************!*\
  !*** ./src/scripts/services/side-sticky-callback.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("// +$(function () {\n//     initSidebar();\n// });\n\n// const initSidebar = () =>{\n//     const main = $(\"#main\");\n//     const callBack = $(\"#sidebar-callback\");\n\n//     setSidebar(callBack, main);\n//     $(document).on('scroll', function () {\n//         setSidebar(callBack, main);\n//     });\n// };\n\n// const setSidebar = (callBack, main) =>{\n//     const defaultMarginBottom = 60 + 106; //106 is a buffer\n//     const bufferBegin = 16;\n\n//     const alertHeight = window.alertHeight ? window.alertHeight : 0;\n//     const fixBegin = $(\"#nav-main\").outerHeight() + $(\"#nav-secondary\").outerHeight() - bufferBegin; //just need nav-bar's height, not interest in alert's height\n//     const fixEnd = main.outerHeight() + main.offset().top - callBack.outerHeight() - defaultMarginBottom - alertHeight - bufferBegin;\n\n//     const scrollTop = $(document).scrollTop();\n\n//     const floatTop = fixBegin + alertHeight;\n//     // console.log(main.outerHeight());\n//     // console.log(\"alert height\", alertHeight);\n//     // console.log(\"floatTop\", floatTop);\n//     // console.log(\"fixBegin\", fixBegin);\n//     // console.log(\"scrollTop\",scrollTop);\n//     // console.log(\"fixEnd\",fixEnd);\n\n//     if (scrollTop > fixBegin && scrollTop < fixEnd) {\n//         // console.log('in');\n//         callBack\n//             .addClass('sidebar-callback__container--fixed')\n//             .css({\"margin-top\": 0, \"top\": floatTop.toString() + \"px\"});\n//             // .css({\"top\": floatTop.toString() + \"px\"});\n//     } else if(scrollTop > fixEnd) {\n//         // console.log('bottom');\n//         callBack\n//             .removeClass('sidebar-callback__container--fixed')\n//             .css({\"margin-top\": 'auto', \"top\": \"0\"});\n//             // .css({\"padding-top\": 'auto'});\n//     } else {\n//         // console.log('above');\n//         callBack\n//             .removeClass('sidebar-callback__container--fixed')\n//             .css({\"margin-top\": 0, \"top\": \"0\"});\n//             // .css({\"top\": 0});\n//     }\n// };\n\n// export {\n//     initSidebar\n// }\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyaXB0cy9zZXJ2aWNlcy9zaWRlLXN0aWNreS1jYWxsYmFjay5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NyYy9zY3JpcHRzL3NlcnZpY2VzL3NpZGUtc3RpY2t5LWNhbGxiYWNrLmpzPzgxZmMiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gKyQoZnVuY3Rpb24gKCkge1xuLy8gICAgIGluaXRTaWRlYmFyKCk7XG4vLyB9KTtcblxuLy8gY29uc3QgaW5pdFNpZGViYXIgPSAoKSA9Pntcbi8vICAgICBjb25zdCBtYWluID0gJChcIiNtYWluXCIpO1xuLy8gICAgIGNvbnN0IGNhbGxCYWNrID0gJChcIiNzaWRlYmFyLWNhbGxiYWNrXCIpO1xuXG4vLyAgICAgc2V0U2lkZWJhcihjYWxsQmFjaywgbWFpbik7XG4vLyAgICAgJChkb2N1bWVudCkub24oJ3Njcm9sbCcsIGZ1bmN0aW9uICgpIHtcbi8vICAgICAgICAgc2V0U2lkZWJhcihjYWxsQmFjaywgbWFpbik7XG4vLyAgICAgfSk7XG4vLyB9O1xuXG4vLyBjb25zdCBzZXRTaWRlYmFyID0gKGNhbGxCYWNrLCBtYWluKSA9Pntcbi8vICAgICBjb25zdCBkZWZhdWx0TWFyZ2luQm90dG9tID0gNjAgKyAxMDY7IC8vMTA2IGlzIGEgYnVmZmVyXG4vLyAgICAgY29uc3QgYnVmZmVyQmVnaW4gPSAxNjtcblxuLy8gICAgIGNvbnN0IGFsZXJ0SGVpZ2h0ID0gd2luZG93LmFsZXJ0SGVpZ2h0ID8gd2luZG93LmFsZXJ0SGVpZ2h0IDogMDtcbi8vICAgICBjb25zdCBmaXhCZWdpbiA9ICQoXCIjbmF2LW1haW5cIikub3V0ZXJIZWlnaHQoKSArICQoXCIjbmF2LXNlY29uZGFyeVwiKS5vdXRlckhlaWdodCgpIC0gYnVmZmVyQmVnaW47IC8vanVzdCBuZWVkIG5hdi1iYXIncyBoZWlnaHQsIG5vdCBpbnRlcmVzdCBpbiBhbGVydCdzIGhlaWdodFxuLy8gICAgIGNvbnN0IGZpeEVuZCA9IG1haW4ub3V0ZXJIZWlnaHQoKSArIG1haW4ub2Zmc2V0KCkudG9wIC0gY2FsbEJhY2sub3V0ZXJIZWlnaHQoKSAtIGRlZmF1bHRNYXJnaW5Cb3R0b20gLSBhbGVydEhlaWdodCAtIGJ1ZmZlckJlZ2luO1xuXG4vLyAgICAgY29uc3Qgc2Nyb2xsVG9wID0gJChkb2N1bWVudCkuc2Nyb2xsVG9wKCk7XG5cbi8vICAgICBjb25zdCBmbG9hdFRvcCA9IGZpeEJlZ2luICsgYWxlcnRIZWlnaHQ7XG4vLyAgICAgLy8gY29uc29sZS5sb2cobWFpbi5vdXRlckhlaWdodCgpKTtcbi8vICAgICAvLyBjb25zb2xlLmxvZyhcImFsZXJ0IGhlaWdodFwiLCBhbGVydEhlaWdodCk7XG4vLyAgICAgLy8gY29uc29sZS5sb2coXCJmbG9hdFRvcFwiLCBmbG9hdFRvcCk7XG4vLyAgICAgLy8gY29uc29sZS5sb2coXCJmaXhCZWdpblwiLCBmaXhCZWdpbik7XG4vLyAgICAgLy8gY29uc29sZS5sb2coXCJzY3JvbGxUb3BcIixzY3JvbGxUb3ApO1xuLy8gICAgIC8vIGNvbnNvbGUubG9nKFwiZml4RW5kXCIsZml4RW5kKTtcblxuLy8gICAgIGlmIChzY3JvbGxUb3AgPiBmaXhCZWdpbiAmJiBzY3JvbGxUb3AgPCBmaXhFbmQpIHtcbi8vICAgICAgICAgLy8gY29uc29sZS5sb2coJ2luJyk7XG4vLyAgICAgICAgIGNhbGxCYWNrXG4vLyAgICAgICAgICAgICAuYWRkQ2xhc3MoJ3NpZGViYXItY2FsbGJhY2tfX2NvbnRhaW5lci0tZml4ZWQnKVxuLy8gICAgICAgICAgICAgLmNzcyh7XCJtYXJnaW4tdG9wXCI6IDAsIFwidG9wXCI6IGZsb2F0VG9wLnRvU3RyaW5nKCkgKyBcInB4XCJ9KTtcbi8vICAgICAgICAgICAgIC8vIC5jc3Moe1widG9wXCI6IGZsb2F0VG9wLnRvU3RyaW5nKCkgKyBcInB4XCJ9KTtcbi8vICAgICB9IGVsc2UgaWYoc2Nyb2xsVG9wID4gZml4RW5kKSB7XG4vLyAgICAgICAgIC8vIGNvbnNvbGUubG9nKCdib3R0b20nKTtcbi8vICAgICAgICAgY2FsbEJhY2tcbi8vICAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnc2lkZWJhci1jYWxsYmFja19fY29udGFpbmVyLS1maXhlZCcpXG4vLyAgICAgICAgICAgICAuY3NzKHtcIm1hcmdpbi10b3BcIjogJ2F1dG8nLCBcInRvcFwiOiBcIjBcIn0pO1xuLy8gICAgICAgICAgICAgLy8gLmNzcyh7XCJwYWRkaW5nLXRvcFwiOiAnYXV0byd9KTtcbi8vICAgICB9IGVsc2Uge1xuLy8gICAgICAgICAvLyBjb25zb2xlLmxvZygnYWJvdmUnKTtcbi8vICAgICAgICAgY2FsbEJhY2tcbi8vICAgICAgICAgICAgIC5yZW1vdmVDbGFzcygnc2lkZWJhci1jYWxsYmFja19fY29udGFpbmVyLS1maXhlZCcpXG4vLyAgICAgICAgICAgICAuY3NzKHtcIm1hcmdpbi10b3BcIjogMCwgXCJ0b3BcIjogXCIwXCJ9KTtcbi8vICAgICAgICAgICAgIC8vIC5jc3Moe1widG9wXCI6IDB9KTtcbi8vICAgICB9XG4vLyB9O1xuXG4vLyBleHBvcnQge1xuLy8gICAgIGluaXRTaWRlYmFyXG4vLyB9XG5cInVzZSBzdHJpY3RcIjsiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/scripts/services/side-sticky-callback.js\n");

/***/ }),

/***/ 11:
/*!************************************************************!*\
  !*** multi ./src/scripts/services/side-sticky-callback.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/xiaotianhuang/Sites/localhost/_yump/wvphn/src/scripts/services/side-sticky-callback.js */"./src/scripts/services/side-sticky-callback.js");


/***/ })

/******/ });