/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/scripts/services/alert.js":
/*!***************************************!*\
  !*** ./src/scripts/services/alert.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n    value: true\n});\nvar alertSpacer = $('.header__spacer--alert');\nvar alertContainer = $('.site-wide-alert');\nvar alertID = void 0;\n\nvar initAlert = function initAlert(alertId) {\n    setAlertId(alertId);\n    // Check if alert has been closed\n    if (getCookie('alert-' + alertID) !== 'closed') {\n        var alertBlock = jQuery('[data-alert=\"' + alertID + '\"]');\n        alertBlock.css({ display: 'flex' }).show();\n        alertSpacer.css({ display: 'flex' }).show();\n\n        updateSpacerHeight();\n        //update height when resize\n        window.addEventListener('resize', function () {\n            updateSpacerHeight();\n        });\n    } else {\n        window.alertHeight = 0;\n    }\n};\n\nvar updateSpacerHeight = function updateSpacerHeight() {\n    var alertHeight = alertContainer.outerHeight();\n    window.alertHeight = alertHeight;\n    alertSpacer.css({\n        'padding-top': alertHeight\n    });\n};\n\nvar setAlertId = function setAlertId(alertId) {\n    alertID = alertId;\n};\n\nvar closeAlert = function closeAlert(alertID) {\n    setCookie('alert-' + alertID, 'closed', 2);\n    $('[data-alert=\"' + alertID + '\"]').slideUp();\n};\n\nvar setCookie = function setCookie(key, value, expiry) {\n    var expires = new Date();\n    expires.setTime(expires.getTime() + expiry * 24 * 60 * 60 * 1000);\n    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();\n};\n\nvar getCookie = function getCookie(key) {\n    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');\n    return keyValue ? keyValue[2] : null;\n};\n\nvar eraseCookie = function eraseCookie(key) {\n    var keyValue = getCookie(key);\n    setCookie(key, keyValue, '-1');\n};\n\nexports.alertContainer = alertContainer;\nexports.alertSpacer = alertSpacer;\nexports.setAlertId = setAlertId;\nexports.updateSpacerHeight = updateSpacerHeight;\nexports.initAlert = initAlert;\nexports.closeAlert = closeAlert;//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2NyaXB0cy9zZXJ2aWNlcy9hbGVydC5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NyYy9zY3JpcHRzL3NlcnZpY2VzL2FsZXJ0LmpzPzhlOTQiXSwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgICB2YWx1ZTogdHJ1ZVxufSk7XG52YXIgYWxlcnRTcGFjZXIgPSAkKCcuaGVhZGVyX19zcGFjZXItLWFsZXJ0Jyk7XG52YXIgYWxlcnRDb250YWluZXIgPSAkKCcuc2l0ZS13aWRlLWFsZXJ0Jyk7XG52YXIgYWxlcnRJRCA9IHZvaWQgMDtcblxudmFyIGluaXRBbGVydCA9IGZ1bmN0aW9uIGluaXRBbGVydChhbGVydElkKSB7XG4gICAgc2V0QWxlcnRJZChhbGVydElkKTtcbiAgICAvLyBDaGVjayBpZiBhbGVydCBoYXMgYmVlbiBjbG9zZWRcbiAgICBpZiAoZ2V0Q29va2llKCdhbGVydC0nICsgYWxlcnRJRCkgIT09ICdjbG9zZWQnKSB7XG4gICAgICAgIHZhciBhbGVydEJsb2NrID0galF1ZXJ5KCdbZGF0YS1hbGVydD1cIicgKyBhbGVydElEICsgJ1wiXScpO1xuICAgICAgICBhbGVydEJsb2NrLmNzcyh7IGRpc3BsYXk6ICdmbGV4JyB9KS5zaG93KCk7XG4gICAgICAgIGFsZXJ0U3BhY2VyLmNzcyh7IGRpc3BsYXk6ICdmbGV4JyB9KS5zaG93KCk7XG5cbiAgICAgICAgdXBkYXRlU3BhY2VySGVpZ2h0KCk7XG4gICAgICAgIC8vdXBkYXRlIGhlaWdodCB3aGVuIHJlc2l6ZVxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcigncmVzaXplJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdXBkYXRlU3BhY2VySGVpZ2h0KCk7XG4gICAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIHdpbmRvdy5hbGVydEhlaWdodCA9IDA7XG4gICAgfVxufTtcblxudmFyIHVwZGF0ZVNwYWNlckhlaWdodCA9IGZ1bmN0aW9uIHVwZGF0ZVNwYWNlckhlaWdodCgpIHtcbiAgICB2YXIgYWxlcnRIZWlnaHQgPSBhbGVydENvbnRhaW5lci5vdXRlckhlaWdodCgpO1xuICAgIHdpbmRvdy5hbGVydEhlaWdodCA9IGFsZXJ0SGVpZ2h0O1xuICAgIGFsZXJ0U3BhY2VyLmNzcyh7XG4gICAgICAgICdwYWRkaW5nLXRvcCc6IGFsZXJ0SGVpZ2h0XG4gICAgfSk7XG59O1xuXG52YXIgc2V0QWxlcnRJZCA9IGZ1bmN0aW9uIHNldEFsZXJ0SWQoYWxlcnRJZCkge1xuICAgIGFsZXJ0SUQgPSBhbGVydElkO1xufTtcblxudmFyIGNsb3NlQWxlcnQgPSBmdW5jdGlvbiBjbG9zZUFsZXJ0KGFsZXJ0SUQpIHtcbiAgICBzZXRDb29raWUoJ2FsZXJ0LScgKyBhbGVydElELCAnY2xvc2VkJywgMik7XG4gICAgJCgnW2RhdGEtYWxlcnQ9XCInICsgYWxlcnRJRCArICdcIl0nKS5zbGlkZVVwKCk7XG59O1xuXG52YXIgc2V0Q29va2llID0gZnVuY3Rpb24gc2V0Q29va2llKGtleSwgdmFsdWUsIGV4cGlyeSkge1xuICAgIHZhciBleHBpcmVzID0gbmV3IERhdGUoKTtcbiAgICBleHBpcmVzLnNldFRpbWUoZXhwaXJlcy5nZXRUaW1lKCkgKyBleHBpcnkgKiAyNCAqIDYwICogNjAgKiAxMDAwKTtcbiAgICBkb2N1bWVudC5jb29raWUgPSBrZXkgKyAnPScgKyB2YWx1ZSArICc7ZXhwaXJlcz0nICsgZXhwaXJlcy50b1VUQ1N0cmluZygpO1xufTtcblxudmFyIGdldENvb2tpZSA9IGZ1bmN0aW9uIGdldENvb2tpZShrZXkpIHtcbiAgICB2YXIga2V5VmFsdWUgPSBkb2N1bWVudC5jb29raWUubWF0Y2goJyhefDspID8nICsga2V5ICsgJz0oW147XSopKDt8JCknKTtcbiAgICByZXR1cm4ga2V5VmFsdWUgPyBrZXlWYWx1ZVsyXSA6IG51bGw7XG59O1xuXG52YXIgZXJhc2VDb29raWUgPSBmdW5jdGlvbiBlcmFzZUNvb2tpZShrZXkpIHtcbiAgICB2YXIga2V5VmFsdWUgPSBnZXRDb29raWUoa2V5KTtcbiAgICBzZXRDb29raWUoa2V5LCBrZXlWYWx1ZSwgJy0xJyk7XG59O1xuXG5leHBvcnRzLmFsZXJ0Q29udGFpbmVyID0gYWxlcnRDb250YWluZXI7XG5leHBvcnRzLmFsZXJ0U3BhY2VyID0gYWxlcnRTcGFjZXI7XG5leHBvcnRzLnNldEFsZXJ0SWQgPSBzZXRBbGVydElkO1xuZXhwb3J0cy51cGRhdGVTcGFjZXJIZWlnaHQgPSB1cGRhdGVTcGFjZXJIZWlnaHQ7XG5leHBvcnRzLmluaXRBbGVydCA9IGluaXRBbGVydDtcbmV4cG9ydHMuY2xvc2VBbGVydCA9IGNsb3NlQWxlcnQ7Il0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/scripts/services/alert.js\n");

/***/ }),

/***/ 9:
/*!*********************************************!*\
  !*** multi ./src/scripts/services/alert.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/xiaotianhuang/Sites/localhost/_yump/wvphn/src/scripts/services/alert.js */"./src/scripts/services/alert.js");


/***/ })

/******/ });