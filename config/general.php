<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see \craft\config\GeneralConfig
 */

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 1,

        // Whether generated URLs should omit "index.php"
        'omitScriptNameInUrls' => true,

        // error page templates will be located under template root with '_' as prefix
        'errorTemplatePrefix' => "_errors/",

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        // Whether Craft should allow system and plugin updates in the Control Panel, and plugin installation from the Plugin Store.
        'allowUpdates' => false,

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),

        // Whether to save the project config out to config/project.yaml
        // (see https://docs.craftcms.com/v3/project-config.html)
        'useProjectConfigFile' => false,

        /**
         * Explicitly set the @web alias to avoid cache poisoning attach.
         * Remember to set WEB_ROOT_URL in your .env file (e.g. https://yump.com.au), without the slash at the end
         *
         * Ref:
         * - https://craftcms.com/knowledge-base/securing-craft#explicitly-set-the-web-alias-for-the-site
         * - https://craftcms.com/docs/3.x/config/#aliases
         */
        'aliases' => [
            '@web' => craft\helpers\App::env('DEFAULT_SITE_URL'),
        ],

        // if not provided here, Craft will use the one set in CMS
        'siteUrl' => sprintf(
            "%s://%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME']
        ),

        // Enable CSRF Protection (recommended)
        'enableCsrfProtection' => true,

        // login path
        'loginPath' => 'admin/login',

        // 'errorTemplatePrefix' => "_errors/",

        // 'defaultImageQuality' => 100,

        'yumpCurlCacheFolder' => CRAFT_BASE_PATH . '/yump/private/cache/curl',

        // custom script permission control
        'controllerActionsPassword' => 'QUpX67EkiETU7Rg',
        'controllerActionsAllowedIps' => array(
            '127.0.0.1', 
            '203.191.201.182',        // Epic office
            '202.91.44.62',           // depo8
            '43.241.54.230',          // staging.yump.com.au (new server)
        ),

        // Card default settings
        'imageCardImagerDefaultSettings' => array(
            'width' => 1076,
            'ratio' => 16/9,
        ),
        'defaultCardCtaText' => 'View',
        'imageCardPlaceholderUrl' => false, // if path provided, we will use the placeholder image when card image is missing. The placeholder image's dimension should follow the 'imageCardImagerDefaultSettings'

        // cache
        'siteNavigationCacheKey' => 'site-navigation',
        'supportOptionConfigKey' => 'support-option',
        'navShouldIncludeHomepage' => true, // if set to true, then the cached navigation content will include homepage at the front of the navItems list, when running the getNav() function under YumpModuleService. Please note, if we don't have homepage in the nav list, then the breadcrumb should include Home crumb; otherwise it shouldn't.

        // social media
        // define which social media links are shown. These URLs must be defined in SEOMatic under Social Media -> 'Same as Links' in order to display.
        'activeSocialMediaIcons' => array(
            array(
                'handle' => 'facebook',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'facebook-square',
            ),
            array(
                'handle' => 'twitter',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'twitter-square',
            ),
            array(
                'handle' => 'youtube',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'youtube-square',
            ),
            array(
                'handle' => 'instagram',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'instagram',
            ),
            array(
                'handle' => 'linkedin',
                'useFontAwesome' => true,
                'fontAwesomeClass' => 'linkedin',
            ),
        ),

        // search filters mapping (manually put in the IDs by checking the CMS)
        'searchFiltersMapping' => array(
            'resource' => array(
                'type' => 'entryType',
                'ids' => [11],
            ),
            'support' => array(
                'type' => 'entryType',
                'ids' => [10],
            ),
            'others' => array(
                'type' => 'entryType',
                'ids' => [2],
            )
//            'documents' => array(
//                'type' => 'assetVolume',
//                'ids' => [8],
//            ),
        ),
        'searchPaginationPerPage' => 7,

        //resourceCategory, deliveryMethod, referralType, fees
        'searchTagFieldIds' => [175, 110, 111, 170],
        //External Resource, Link Only Page are external so we do target="_blank" in front end
        'externalEntryTypeIds' => [17, 3],

        'supportOptionEntrySectionHandle' => 'supportOptions',
        'resourceLibraryEntrySectionHandle' => 'resourceLibrary',

        // what's the handle for the tags field, for entries and documents.
        'searchTagFieldHandle' => 'tags',
        'googleMapApiKey' =>  getenv('GOOGLE_MAP_SECRET'),
    ],

    // Dev environment settings
    'dev' => [
        // Dev Mode (see https://craftcms.com/guides/what-dev-mode-does)
        'devMode' => empty($_GET['devMode']),
        'allowUpdates' => true,
        'testToEmailAddress' => array(
            'devs@yump.com.au' => 'Yump Devs'
        ),
    ],

    // Staging environment settings
    'staging' => [
        // Set this to `false` to prevent administrative changes from being made on staging
        'allowAdminChanges' => true,

        // Dev Mode (enabled when ?devMode=1 querystring is present)
        'devMode' => !empty($_GET['devMode']),
        'enableLoginWallOnDocs' => true,
    ],

    // Production environment settings
    'production' => [
        // Set this to `false` to prevent administrative changes from being made on production
        'allowAdminChanges' => true,

        'enableLoginWallOnDocs' => true,

        'googleMapApiKey' => 'AIzaSyC86_bVjGBPCe2EKF2_BXLT4YDAVkhgZoU',
    ],
];
