<?php
/**
 * https://nystudio107.com/docs/seomatic/Configuring.html#multi-environment-config-settings
 */

return [
    // Global settings
    '*' => [
        
    ],

    // Dev environment settings
    'dev' => [
        'environment' => 'local',
    ],

    // Staging environment settings
    'staging' => [
        'environment' => 'live',
    ],

    // Production environment settings
    'production' => [
        // The server environment, either `live`, `staging`, or `local`
        'environment' => 'live',
    ],
];
